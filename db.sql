/*
SQLyog Community v13.1.7 (64 bit)
MySQL - 10.4.17-MariaDB : Database - hrapp
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`hrapp` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `hrapp`;

/*Table structure for table `aktivnost` */

DROP TABLE IF EXISTS `aktivnost`;

CREATE TABLE `aktivnost` (
  `sifraProjekta` varchar(20) NOT NULL,
  `sifraAktivnosti` varchar(20) NOT NULL,
  `nazivAktivnosti` varchar(50) DEFAULT NULL,
  `opisAktivnosti` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`sifraProjekta`,`sifraAktivnosti`),
  CONSTRAINT `fk_sifraProjekta` FOREIGN KEY (`sifraProjekta`) REFERENCES `projekat` (`sifraProjekta`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `aktivnost` */

insert  into `aktivnost`(`sifraProjekta`,`sifraAktivnosti`,`nazivAktivnosti`,`opisAktivnosti`) values 
('fonklame2021','video','Izrada videa za Fece reklama','Prikupljanje materijala i izrada celog videa koji ce biti prikazivan u bioskopu za Vece reklama'),
('fonup18','logistika','Celokupna logisticka podrska na dan projekta','Priprema sala, docek delegata i panelista, raspored repro materijala itd'),
('fonup18','objave','Kreiranje objava za drustvene mreze','Kreiranje objava za Facebook, Instagram, LinkedIn'),
('fonup18','sajt','Izrada sajta','Front i back sajta, kreiranje formi za prijave i evaluacije'),
('marfi18','objave','Kreiranje objava za drustvene mreze','Instagram, Facebook, LinkedIn'),
('marfi18','prijave','Odabir delegata','Pregledanje priajva i komunikacija sa delegatima'),
('vs2021','sajt','Uredjenje sajta','Front, Back, Forme');

/*Table structure for table `clan_organizacije` */

DROP TABLE IF EXISTS `clan_organizacije`;

CREATE TABLE `clan_organizacije` (
  `clanID` bigint(11) NOT NULL AUTO_INCREMENT,
  `imePrezime` varchar(50) DEFAULT NULL,
  `datumUclanjenja` date DEFAULT NULL,
  `datumIsclanjenja` date DEFAULT NULL,
  `sifraSektora` varchar(10) DEFAULT NULL,
  `kontakt` varchar(30) DEFAULT NULL,
  `brojBodova` int(11) DEFAULT NULL,
  PRIMARY KEY (`clanID`),
  KEY `fk_sektor` (`sifraSektora`),
  CONSTRAINT `fk_sifraSektora` FOREIGN KEY (`sifraSektora`) REFERENCES `sektor` (`sifraSektora`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=utf8;

/*Data for the table `clan_organizacije` */

insert  into `clan_organizacije`(`clanID`,`imePrezime`,`datumUclanjenja`,`datumIsclanjenja`,`sifraSektora`,`kontakt`,`brojBodova`) values 
(1,'Marta Maric','2021-03-12',NULL,'HR',NULL,0),
(29,'Ivana Jovanovic','2019-12-23','2020-12-23','HR','061/111-7866',7),
(30,'Marko Markovic','2021-01-01',NULL,'HR','062/855-3666',8),
(32,'Maja Martic','2020-01-01','2021-01-01','ER','065/552-9987',9),
(33,'Mihajlo Ninkovic','2017-10-11',NULL,'IT','062/277-3689',0),
(34,'Borivoje Mitic','2013-01-15','2019-02-01','IT','069/9852-333',12),
(35,'Jasna Markovic','2019-12-13',NULL,'IT','063/389-6555',0),
(36,'Mina Popovic','2017-09-15',NULL,'ER','064/789-3322',3),
(37,'Jovana Jokic','2013-12-01',NULL,'M&PR','062/258-3666',3),
(38,'Janko Jankovic','2017-12-12',NULL,'Dizajn','062/859-9635',3),
(39,'Paja Jovanovic','2013-01-01',NULL,'Dizajn','062/285-9933',3),
(40,'Ivan Mojsilovic','2001-01-01',NULL,'M&PR','062/285-5588',11),
(41,'Jovana Marinkovic','2019-10-25',NULL,'M&PR','064/128-5567',11);

/*Table structure for table `projekat` */

DROP TABLE IF EXISTS `projekat`;

CREATE TABLE `projekat` (
  `sifraProjekta` varchar(20) NOT NULL,
  `nazivProjekta` varchar(50) DEFAULT NULL,
  `datumOdrzavanja` date DEFAULT NULL,
  `koordinatorProjektaID` bigint(11) DEFAULT NULL,
  PRIMARY KEY (`sifraProjekta`),
  KEY `fk_koordinatorProjekta` (`koordinatorProjektaID`),
  CONSTRAINT `fk_koordinatorProjektaID` FOREIGN KEY (`koordinatorProjektaID`) REFERENCES `clan_organizacije` (`clanID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `projekat` */

insert  into `projekat`(`sifraProjekta`,`nazivProjekta`,`datumOdrzavanja`,`koordinatorProjektaID`) values 
('fonklame2021','FONklame 2021','2021-03-04',41),
('fonup18','FONup 2018','2018-10-10',33),
('marfi18','Znanjem protiv Marfija 2018','2018-11-13',36),
('vs2021','Vruca Stolica 2021','2021-11-11',30);

/*Table structure for table `sektor` */

DROP TABLE IF EXISTS `sektor`;

CREATE TABLE `sektor` (
  `sifraSektora` varchar(10) NOT NULL,
  `punNaziv` varchar(50) DEFAULT NULL,
  `koordinatorSektoraID` bigint(11) DEFAULT NULL,
  PRIMARY KEY (`sifraSektora`),
  KEY `fk_koordinatorSektora` (`koordinatorSektoraID`),
  CONSTRAINT `fk_koordinatorSektoraID` FOREIGN KEY (`koordinatorSektoraID`) REFERENCES `clan_organizacije` (`clanID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `sektor` */

insert  into `sektor`(`sifraSektora`,`punNaziv`,`koordinatorSektoraID`) values 
('Dizajn','Sektor za dizajn',39),
('ER','Sektor za spoljne odnose',36),
('HR','Sektor za ljudske resurse',1),
('IT','Sektor za informacione tehnologije',33),
('M&PR','Sektor za marketing i odnose sa javnoscu',37);

/*Table structure for table `user` */

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `id` bigint(11) NOT NULL,
  `firstname` varchar(50) NOT NULL,
  `lastname` varchar(50) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `user` */

insert  into `user`(`id`,`firstname`,`lastname`,`username`,`password`) values 
(1,'Anja','Kapikul','anja','anja'),
(2,'Petar','Petrovic','p','p');

/*Table structure for table `zadatak` */

DROP TABLE IF EXISTS `zadatak`;

CREATE TABLE `zadatak` (
  `sifraProjekta` varchar(20) NOT NULL,
  `sifraAktivnosti` varchar(20) NOT NULL,
  `izvrsiteljID` bigint(11) NOT NULL,
  `opisZadatka` varchar(200) DEFAULT NULL,
  `brojBodova` int(5) DEFAULT NULL,
  PRIMARY KEY (`sifraProjekta`,`sifraAktivnosti`,`izvrsiteljID`),
  KEY `fk_sifraAktivnostiZ` (`sifraAktivnosti`),
  KEY `fk_izvrsiteljID` (`izvrsiteljID`),
  CONSTRAINT `fk_izvrsiteljID` FOREIGN KEY (`izvrsiteljID`) REFERENCES `clan_organizacije` (`clanID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_sifraProjektaZ` FOREIGN KEY (`sifraProjekta`) REFERENCES `aktivnost` (`sifraProjekta`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `zadatak` */

insert  into `zadatak`(`sifraProjekta`,`sifraAktivnosti`,`izvrsiteljID`,`opisZadatka`,`brojBodova`) values 
('fonklame2021','video',34,'Montaza',7),
('fonklame2021','video',40,'Selekcija videa',2),
('fonup18','logistika',1,'Raspored repro materijala u sali B009',4),
('fonup18','logistika',30,'Raspored repro materijala u sali B103',4),
('fonup18','logistika',34,'Tehnicka podrska u sali B103',4),
('fonup18','logistika',37,'Priprema sale B009',3),
('fonup18','logistika',38,'Priprema sale B103',3),
('fonup18','logistika',40,'Tehnicka podrska u sali B009',4),
('fonup18','logistika',41,'Docek delegata na standu ispred sala',5),
('fonup18','objave',30,'Kreiranje objava za Instagram',5),
('fonup18','objave',32,'Kreiranje objava za Facebook',5),
('fonup18','sajt',34,'Front end',5),
('fonup18','sajt',36,'Kreiranje formi za evaluacije',3),
('fonup18','sajt',39,'Kreiranje formi za prijave',3),
('fonup18','sajt',40,'Back end',6),
('marfi18','objave',29,'Objave za Instagram',3),
('marfi18','objave',30,'Objave za Facebook',3),
('marfi18','objave',41,'Storiji',3),
('marfi18','prijave',32,'Slanje mejlova',4),
('marfi18','prijave',41,'Pregledanje prijava',3),
('vs2021','sajt',29,'Back',4),
('vs2021','sajt',40,'Forme',3),
('vs2021','sajt',41,'Front',3);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
