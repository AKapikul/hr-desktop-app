/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs.ac.bg.fon.ps.view.form.components.table;

import java.util.List;
import javax.swing.table.AbstractTableModel;
import rs.ac.bg.fon.ps.domain.ClanOrganizacije;

/**
 *
 * @author Anja
 */
public class ClanTableModel extends AbstractTableModel {

    private List<ClanOrganizacije> clanovi;
    private String[] columnNames = new String[]{"Ime i prezime", "Sektor","Broj bodova"};
    private Class[] columnClass = new Class[]{String.class, String.class, Integer.class};

    public ClanTableModel(List<ClanOrganizacije> clanovi) {
        this.clanovi = clanovi;
    }

    @Override
    public int getRowCount() {
        return clanovi.size();
    }

    @Override
    public int getColumnCount() {
        return columnNames.length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        ClanOrganizacije clan = clanovi.get(rowIndex);
        switch (columnIndex) {
            case 0:
                return clan.getImePrezime();
            case 1:
                return clan.getSektor();
            case 2:
                return clan.getBrojBodova();
            default:
                return "n/a";
        }
    }

    @Override
    public String getColumnName(int column) {
        if (column > columnNames.length) {
            return "n/a";
        }
        return columnNames[column];
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        return columnClass[columnIndex];
    }

    public ClanOrganizacije getClan(int index) {
        return clanovi.get(index);
    }

    public List<ClanOrganizacije> getClanovi() {
        return clanovi;
    }

    public void update(List<ClanOrganizacije> list) {
        clanovi = list;
        fireTableDataChanged();
    }

    public void deleteClan(int rowIndex) {
        clanovi.remove(rowIndex);
    }

    public void deleteClan(ClanOrganizacije cl) {
        for (ClanOrganizacije clanOrganizacije : clanovi) {
            if (clanOrganizacije.equals(cl)) {
                clanovi.remove(clanOrganizacije);
            }
        }
    }

    public void addClan(ClanOrganizacije clan) {
        clanovi.add(clan);
    }



}
