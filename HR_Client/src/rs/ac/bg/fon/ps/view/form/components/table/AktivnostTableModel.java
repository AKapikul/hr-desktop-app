/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs.ac.bg.fon.ps.view.form.components.table;

import java.util.List;
import javax.swing.table.AbstractTableModel;
import rs.ac.bg.fon.ps.domain.Aktivnost;

/**
 *
 * @author Anja
 */
public class AktivnostTableModel extends AbstractTableModel{
    private List<Aktivnost> aktivnosti;   
    private String[] columnNames = new String[]{"Šifra aktivnosti", "Naziv", "Opis"};
    private Class[] columnClass = new Class[]{String.class, String.class, String.class};

    public AktivnostTableModel(List<Aktivnost> aktivnosti) {
        this.aktivnosti = aktivnosti;
    }

    

    @Override
    public int getRowCount() {
        return aktivnosti.size();
    }

    @Override
    public int getColumnCount() {
        return columnNames.length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Aktivnost aktivnost = aktivnosti.get(rowIndex);
        switch(columnIndex){
            case 0: return aktivnost.getSifraAktivnosti();
            case 1: return aktivnost.getNazivAktivnosti();
            case 2: return aktivnost.getOpisAktivnosti();
            default: return "n/a";
        }
    }

     @Override
    public String getColumnName(int column) {
        if (column > columnNames.length) {
            return "n/a";
        }
        return columnNames[column];
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        return columnClass[columnIndex];
    }

    public Aktivnost getAktivnost(int index){
        return aktivnosti.get(index);
    }

    public List<Aktivnost> getAktivnosti() {
        return aktivnosti;
    }
    
    public void update(List<Aktivnost> list){
        aktivnosti = list;
        fireTableDataChanged();
    }
    
    public void deleteAktivnost(int rowIndex){
        aktivnosti.remove(rowIndex);
    }
    
    public void deleteAktivnost(Aktivnost a){
        for(int i = 0; i<aktivnosti.size(); i++){
            if(aktivnosti.get(i).equals(a)){
                aktivnosti.remove(aktivnosti.get(i));
            }
        }
        
        fireTableDataChanged();
    }
    
    public void addAktivnost(Aktivnost a){
        aktivnosti.add(a);
        fireTableDataChanged();
    }
}
