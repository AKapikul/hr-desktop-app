/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs.ac.bg.fon.ps.view.form.components.table;

import java.util.Date;
import java.util.List;
import javax.swing.table.AbstractTableModel;
import rs.ac.bg.fon.ps.domain.Projekat;

/**
 *
 * @author Anja
 */
public class ProjekatTableModel extends AbstractTableModel{
    private List<Projekat> projekti;
    private String[] columnNames = new String[]{"Šifra projekta", "Naziv", "Datum održavanja", "Koordinator"};
    private Class[] columnClass = new Class[]{String.class, String.class, Date.class, Integer.class};

    public ProjekatTableModel(List<Projekat> projekti) {
        this.projekti = projekti;
    }

    
    @Override
    public int getRowCount() {
        return projekti.size();
    }

    @Override
    public int getColumnCount() {
        return columnNames.length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Projekat projekat = projekti.get(rowIndex);
        switch(columnIndex){
            case 0: return projekat.getSifraProjekta();
            case 1: return projekat.getNazivProjekta();
            case 2: return projekat.getDatumOdrzavanja();
            case 3: return projekat.getKoordinatorProjekta();
            default: return "n/a";
        }
    }

    @Override
    public String getColumnName(int column) {
        if (column > columnNames.length) {
            return "n/a";
        }
        return columnNames[column];
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
         return columnClass[columnIndex];
    }
    
    public Projekat getProjekat(int index){
        return projekti.get(index);
    }

    public List<Projekat> getProjekti() {
        return projekti;
    }

    public void update(List<Projekat> list){
        projekti = list;
        fireTableDataChanged();
    }
    
    public void deleteProjekat(int rowIndex){
        projekti.remove(rowIndex);
    }
    
     public void deleteProjekat(Projekat projekat){
         for (Projekat projekat1 : projekti) {
             if(projekat1.equals(projekat)){
                 projekti.remove(projekat1);
             }
         }
     }
    
    public void addProjekat(Projekat projekat){
        projekti.add(projekat);
    }
    
   
    
    
}
