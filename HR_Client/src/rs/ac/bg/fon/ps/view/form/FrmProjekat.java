/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs.ac.bg.fon.ps.view.form;

import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.JTextField;

/**
 *
 * @author Anja
 */
public class FrmProjekat extends javax.swing.JFrame {

    /**
     * Creates new form FrmProjekat
     */
    public FrmProjekat() {
        initComponents();
        setLocationRelativeTo(null);
        setTitle("Human Resources app - pregled projekta");
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane2 = new javax.swing.JScrollPane();
        jTable2 = new javax.swing.JTable();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        lblCurrentUser = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        lblProjekat = new javax.swing.JLabel();
        pnlAktivnosti = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblAktivnosti = new javax.swing.JTable();
        btnDodajNovuAktivnost = new javax.swing.JButton();
        btnPrikaziDetaljeAktivnosti = new javax.swing.JButton();
        btnIzmeniAktivnost = new javax.swing.JButton();
        btnObrisiAktivnost = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        btnOmoguciIzmene = new javax.swing.JButton();
        txtSifraProjekta = new javax.swing.JTextField();
        txtDatumOdrzavanja = new javax.swing.JTextField();
        txtNazivProjekta = new javax.swing.JTextField();
        cbKoordinatorProjekta = new javax.swing.JComboBox<>();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        btnObrisiProjekat = new javax.swing.JButton();
        btnSacuvajIzmene = new javax.swing.JButton();
        btnDodajProjekat = new javax.swing.JButton();

        jTable2.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane2.setViewportView(jTable2);

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanel1.setBackground(new java.awt.Color(159, 192, 194));

        jLabel1.setFont(new java.awt.Font("Verdana", 1, 11)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(0, 102, 102));
        jLabel1.setText("Prijavljeni korisnik: ");

        lblCurrentUser.setFont(new java.awt.Font("Verdana", 1, 11)); // NOI18N
        lblCurrentUser.setForeground(new java.awt.Color(0, 102, 102));

        jLabel2.setFont(new java.awt.Font("Verdana", 0, 10)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(0, 102, 102));
        jLabel2.setText("© Anja Kapikul 2021");

        jLabel9.setFont(new java.awt.Font("Verdana", 1, 11)); // NOI18N
        jLabel9.setForeground(new java.awt.Color(0, 102, 102));
        jLabel9.setText("Uređivanje projekta:");

        lblProjekat.setFont(new java.awt.Font("Verdana", 1, 11)); // NOI18N
        lblProjekat.setForeground(new java.awt.Color(0, 102, 102));
        lblProjekat.setText("projektat");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(lblCurrentUser, javax.swing.GroupLayout.PREFERRED_SIZE, 123, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel2)
                            .addComponent(jLabel1)
                            .addComponent(jLabel9)
                            .addComponent(lblProjekat))
                        .addGap(0, 19, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(lblCurrentUser, javax.swing.GroupLayout.PREFERRED_SIZE, 19, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel9)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(lblProjekat)
                .addGap(18, 18, 18)
                .addComponent(jLabel2)
                .addContainerGap())
        );

        pnlAktivnosti.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Aktivnosti na projektu", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Verdana", 1, 11), new java.awt.Color(0, 102, 102))); // NOI18N

        tblAktivnosti.setFont(new java.awt.Font("Verdana", 0, 11)); // NOI18N
        tblAktivnosti.setForeground(new java.awt.Color(0, 102, 102));
        tblAktivnosti.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(tblAktivnosti);

        btnDodajNovuAktivnost.setFont(new java.awt.Font("Verdana", 1, 11)); // NOI18N
        btnDodajNovuAktivnost.setForeground(new java.awt.Color(0, 102, 102));
        btnDodajNovuAktivnost.setText("Dodaj novu aktivnost");
        btnDodajNovuAktivnost.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDodajNovuAktivnostActionPerformed(evt);
            }
        });

        btnPrikaziDetaljeAktivnosti.setFont(new java.awt.Font("Verdana", 1, 11)); // NOI18N
        btnPrikaziDetaljeAktivnosti.setForeground(new java.awt.Color(0, 102, 102));
        btnPrikaziDetaljeAktivnosti.setText("Prikaži detalje aktivnosti");
        btnPrikaziDetaljeAktivnosti.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPrikaziDetaljeAktivnostiActionPerformed(evt);
            }
        });

        btnIzmeniAktivnost.setFont(new java.awt.Font("Verdana", 1, 11)); // NOI18N
        btnIzmeniAktivnost.setForeground(new java.awt.Color(0, 102, 102));
        btnIzmeniAktivnost.setText("Izmeni podatke o aktivnosti");

        btnObrisiAktivnost.setFont(new java.awt.Font("Verdana", 1, 11)); // NOI18N
        btnObrisiAktivnost.setForeground(new java.awt.Color(0, 102, 102));
        btnObrisiAktivnost.setText("Obriši aktivnost");

        javax.swing.GroupLayout pnlAktivnostiLayout = new javax.swing.GroupLayout(pnlAktivnosti);
        pnlAktivnosti.setLayout(pnlAktivnostiLayout);
        pnlAktivnostiLayout.setHorizontalGroup(
            pnlAktivnostiLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlAktivnostiLayout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 349, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(pnlAktivnostiLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnDodajNovuAktivnost, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnPrikaziDetaljeAktivnosti, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnIzmeniAktivnost, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 220, Short.MAX_VALUE)
                    .addComponent(btnObrisiAktivnost, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
        );
        pnlAktivnostiLayout.setVerticalGroup(
            pnlAktivnostiLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlAktivnostiLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlAktivnostiLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                    .addGroup(pnlAktivnostiLayout.createSequentialGroup()
                        .addComponent(btnDodajNovuAktivnost)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(btnPrikaziDetaljeAktivnosti)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(btnIzmeniAktivnost)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(btnObrisiAktivnost)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jLabel5.setFont(new java.awt.Font("Verdana", 1, 11)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(0, 102, 102));
        jLabel5.setText("Datum održavanja:");

        jLabel6.setFont(new java.awt.Font("Verdana", 1, 11)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(0, 102, 102));
        jLabel6.setText("Koordinator projekta:");

        btnOmoguciIzmene.setFont(new java.awt.Font("Verdana", 1, 11)); // NOI18N
        btnOmoguciIzmene.setForeground(new java.awt.Color(0, 102, 102));
        btnOmoguciIzmene.setText("Omogući izmene");
        btnOmoguciIzmene.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnOmoguciIzmeneActionPerformed(evt);
            }
        });

        txtSifraProjekta.setFont(new java.awt.Font("Verdana", 0, 11)); // NOI18N
        txtSifraProjekta.setForeground(new java.awt.Color(0, 102, 102));
        txtSifraProjekta.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtSifraProjektaActionPerformed(evt);
            }
        });

        txtDatumOdrzavanja.setFont(new java.awt.Font("Verdana", 0, 11)); // NOI18N
        txtDatumOdrzavanja.setForeground(new java.awt.Color(0, 102, 102));

        txtNazivProjekta.setFont(new java.awt.Font("Verdana", 0, 11)); // NOI18N
        txtNazivProjekta.setForeground(new java.awt.Color(0, 102, 102));

        cbKoordinatorProjekta.setFont(new java.awt.Font("Verdana", 0, 11)); // NOI18N
        cbKoordinatorProjekta.setForeground(new java.awt.Color(0, 102, 102));
        cbKoordinatorProjekta.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        jLabel3.setFont(new java.awt.Font("Verdana", 1, 11)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(0, 102, 102));
        jLabel3.setText("Šifra projekta:");

        jLabel4.setFont(new java.awt.Font("Verdana", 1, 11)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(0, 102, 102));
        jLabel4.setText("Naziv projekta:");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel6)
                        .addGap(18, 18, 18)
                        .addComponent(cbKoordinatorProjekta, javax.swing.GroupLayout.PREFERRED_SIZE, 265, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(btnOmoguciIzmene))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel4)
                            .addComponent(jLabel3))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(txtSifraProjekta, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jLabel5)
                                .addGap(18, 18, 18)
                                .addComponent(txtDatumOdrzavanja))
                            .addComponent(txtNazivProjekta, javax.swing.GroupLayout.PREFERRED_SIZE, 463, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtSifraProjekta, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel5)
                    .addComponent(txtDatumOdrzavanja, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(txtNazivProjekta, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(btnOmoguciIzmene)
                    .addComponent(cbKoordinatorProjekta, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        btnObrisiProjekat.setFont(new java.awt.Font("Verdana", 1, 11)); // NOI18N
        btnObrisiProjekat.setForeground(new java.awt.Color(0, 102, 102));
        btnObrisiProjekat.setText("Obriši projekat");
        btnObrisiProjekat.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnObrisiProjekatActionPerformed(evt);
            }
        });

        btnSacuvajIzmene.setFont(new java.awt.Font("Verdana", 1, 11)); // NOI18N
        btnSacuvajIzmene.setForeground(new java.awt.Color(0, 102, 102));
        btnSacuvajIzmene.setText("Sačuvaj izmene");
        btnSacuvajIzmene.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSacuvajIzmeneActionPerformed(evt);
            }
        });

        btnDodajProjekat.setFont(new java.awt.Font("Verdana", 1, 11)); // NOI18N
        btnDodajProjekat.setForeground(new java.awt.Color(0, 102, 102));
        btnDodajProjekat.setText("Dodaj projekat");
        btnDodajProjekat.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDodajProjekatActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addComponent(btnObrisiProjekat, javax.swing.GroupLayout.PREFERRED_SIZE, 183, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 30, Short.MAX_VALUE)
                .addComponent(btnSacuvajIzmene, javax.swing.GroupLayout.PREFERRED_SIZE, 183, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(btnDodajProjekat, javax.swing.GroupLayout.PREFERRED_SIZE, 183, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnObrisiProjekat)
                    .addComponent(btnSacuvajIzmene)
                    .addComponent(btnDodajProjekat)))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(20, 20, 20)
                        .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(pnlAktivnosti, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(11, 11, 11)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(1, 1, 1)
                        .addComponent(pnlAktivnosti, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void txtSifraProjektaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtSifraProjektaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtSifraProjektaActionPerformed

    private void btnPrikaziDetaljeAktivnostiActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPrikaziDetaljeAktivnostiActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnPrikaziDetaljeAktivnostiActionPerformed

    private void btnOmoguciIzmeneActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnOmoguciIzmeneActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnOmoguciIzmeneActionPerformed

    private void btnObrisiProjekatActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnObrisiProjekatActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnObrisiProjekatActionPerformed

    private void btnSacuvajIzmeneActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSacuvajIzmeneActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnSacuvajIzmeneActionPerformed

    private void btnDodajProjekatActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDodajProjekatActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnDodajProjekatActionPerformed

    private void btnDodajNovuAktivnostActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDodajNovuAktivnostActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnDodajNovuAktivnostActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(FrmProjekat.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(FrmProjekat.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(FrmProjekat.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(FrmProjekat.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new FrmProjekat().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnDodajNovuAktivnost;
    private javax.swing.JButton btnDodajProjekat;
    private javax.swing.JButton btnIzmeniAktivnost;
    private javax.swing.JButton btnObrisiAktivnost;
    private javax.swing.JButton btnObrisiProjekat;
    private javax.swing.JButton btnOmoguciIzmene;
    private javax.swing.JButton btnPrikaziDetaljeAktivnosti;
    private javax.swing.JButton btnSacuvajIzmene;
    private javax.swing.JComboBox<Object> cbKoordinatorProjekta;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTable jTable2;
    private javax.swing.JLabel lblCurrentUser;
    private javax.swing.JLabel lblProjekat;
    private javax.swing.JPanel pnlAktivnosti;
    private javax.swing.JTable tblAktivnosti;
    private javax.swing.JTextField txtDatumOdrzavanja;
    private javax.swing.JTextField txtNazivProjekta;
    private javax.swing.JTextField txtSifraProjekta;
    // End of variables declaration//GEN-END:variables

    public JLabel getLblCurrentUser() {
        return lblCurrentUser;
    }

    public JButton getBtnDodajNovuAktivnost() {
        return btnDodajNovuAktivnost;
    }

    public JButton getBtnDodajProjekat() {
        return btnDodajProjekat;
    }

    public JButton getBtnIzmeniAktivnost() {
        return btnIzmeniAktivnost;
    }

    public JButton getBtnObrisiAktivnost() {
        return btnObrisiAktivnost;
    }

    public JButton getBtnObrisiProjekat() {
        return btnObrisiProjekat;
    }

    public JButton getBtnPrikaziDetaljeAktivnosti() {
        return btnPrikaziDetaljeAktivnosti;
    }

    public JButton getBtnSacuvajIzmene() {
        return btnSacuvajIzmene;
    }

    public JTextField getTxtDatumOdrzavanja() {
        return txtDatumOdrzavanja;
    }

    public JComboBox<Object> getCbKoordinatorProjekta() {
        return cbKoordinatorProjekta;
    }

  

    public JTextField getTxtNazivProjekta() {
        return txtNazivProjekta;
    }

    public JTable getTblAktivnosti() {
        return tblAktivnosti;
    }

    public JTextField getTxtSifraProjekta() {
        return txtSifraProjekta;
    }

    public JPanel getPnlAktivnosti() {
        return pnlAktivnosti;
    }

    public JLabel getLblProjekat() {
        return lblProjekat;
    }
    
    
    

    public JButton getBtnOmoguciIzmene() {
        return btnOmoguciIzmene;
    }

    public void addBtnDodajNovuAktivnostActionListener(ActionListener actionListener) {
        btnDodajNovuAktivnost.addActionListener(actionListener);
    }

    public void addBtnIzmeniAktivnostActionListener(ActionListener actionListener) {
        btnIzmeniAktivnost.addActionListener(actionListener);
    }

    public void addBtnObrisiAktivnostActionListener(ActionListener actionListener) {
        btnObrisiAktivnost.addActionListener(actionListener);
    }

    public void addBtnPrikaziDetaljeAktivnostiActionListener(ActionListener actionListener) {
        btnPrikaziDetaljeAktivnosti.addActionListener(actionListener);
    }

    public void addBtnDodajProjekatActionListener(ActionListener actionListener) {
        btnDodajProjekat.addActionListener(actionListener);
    }

    public void addBtnObrisiProjekatActionListener(ActionListener actionListener) {
        btnObrisiProjekat.addActionListener(actionListener);
    }

    public void addBtnSacuvajIzmeneActionListener(ActionListener actionListener) {
        btnSacuvajIzmene.addActionListener(actionListener);
    }

    public void addBtnOmoguciIzmeneActionListener(ActionListener actionListener) {
        btnOmoguciIzmene.addActionListener(actionListener);
    }
    
    

    

    
}
