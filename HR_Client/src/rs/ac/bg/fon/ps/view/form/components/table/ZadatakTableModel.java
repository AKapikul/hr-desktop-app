/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs.ac.bg.fon.ps.view.form.components.table;

import java.util.List;
import javax.swing.table.AbstractTableModel;
import rs.ac.bg.fon.ps.domain.Zadatak;

/**
 *
 * @author Anja
 */
public class ZadatakTableModel extends AbstractTableModel {
    private List<Zadatak> zadaci;
    private String[] columnNames = new String[]{"Opis zadatka", "Izvršitelj", "Broj bodova"};
    private Class[] columnClass = new Class[]{String.class, String.class, Integer.class};

    public ZadatakTableModel(List<Zadatak> zadaci) {
        this.zadaci = zadaci;
    }

    
    
    @Override
    public int getRowCount() {
        return zadaci.size();
    }

    @Override
    public int getColumnCount() {
        return columnNames.length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Zadatak zadatak = zadaci.get(rowIndex);
        switch(columnIndex){
            case 0: return zadatak.getOpisZadatka();
            case 1: return zadatak.getIzvrsitelj();
            case 2: return zadatak.getBrojBodova();
            default: return "n/a";
        }
    }
    
    @Override
    public String getColumnName(int column) {
        if (column > columnNames.length) {
            return "n/a";
        }
        return columnNames[column];
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
         return columnClass[columnIndex];
    }

    public List<Zadatak> getZadaci() {
        return zadaci;
    }
    
    public Zadatak getZadatak(int index){
        return zadaci.get(index);
    }
    
     public void update(List<Zadatak> list){
        zadaci = list;
        fireTableDataChanged();
    }
    
    public void deleteZadatak(int rowIndex){
        zadaci.remove(rowIndex);
    }
    
     public void deleteZadatak(Zadatak zadatak){
         for (Zadatak zadatak1 : zadaci) {
             if(zadatak1.equals(zadatak)){
                 zadaci.remove(zadatak1);
                 break;
             }
         }
         fireTableDataChanged();
     }
    
    public void addZadatak(Zadatak zadatak){
        zadaci.add(zadatak);
        fireTableDataChanged();
    }
}
