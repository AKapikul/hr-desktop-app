/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs.ac.bg.fon.ps.communication;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.swing.JOptionPane;
import rs.ac.bg.fon.ps.domain.Aktivnost;
import rs.ac.bg.fon.ps.domain.ClanOrganizacije;
import rs.ac.bg.fon.ps.domain.IDomainObject;
import rs.ac.bg.fon.ps.domain.Projekat;
import rs.ac.bg.fon.ps.domain.Sektor;
import rs.ac.bg.fon.ps.domain.User;
import rs.ac.bg.fon.ps.domain.Zadatak;

/**
 *
 * @author Anja
 */
public class Communication {

    private static Communication instance;
    private Socket socket;
    ObjectOutputStream oos;
    ObjectInputStream ois;

    private Communication() {
    }

    public void connect() {
        try {
            socket = new Socket("localhost", 9000);
            oos = new ObjectOutputStream(socket.getOutputStream());
            ois = new ObjectInputStream(socket.getInputStream());
            System.out.println("Povezao se sa serverom");
        } catch (IOException ex) {
            JOptionPane.showMessageDialog(null, "Dogodila se greska u komunikaciji!(server je ugasen)");
            System.exit(0);
            System.out.println("Greska u povezivanju! (server nije upaljen)");
        }
    }

    public static Communication getInstance() {
        if (instance == null) {
            instance = new Communication();
        }
        return instance;
    }

    public void sendRequest(Request requestObject) {
        try {
            oos.writeObject(requestObject);
        } catch (IOException ex) {
            System.out.println("Greska u komunikaciji!");
        }
    }

    public Response receiveResponse() {
        Response responseObject = new Response();
        try {
            responseObject = (Response) ois.readObject();
        } catch (IOException ex) {
            System.out.println("Greska u komunikaciji!");
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
        }
        return responseObject;
    }

    public User login(User user) throws IOException, ClassNotFoundException, Exception {
        Request request = new Request();

        request.setOperation(Operation.LOGIN);
        request.setArgument(user);

        Communication.getInstance().sendRequest(request);
        Response response = Communication.getInstance().receiveResponse();
        if (response.getResult() != null) {
            User user1 = (User) response.getResult();
            return user1;

        } else {
            throw new Exception("Ne postoji dati korisnik");
        }
    }

    public List<Sektor> getAllSektor() throws Exception {
        Request request = new Request();
        request.setOperation(Operation.GET_ALL_SEKTORI);

        Communication.getInstance().sendRequest(request);

        Response response = Communication.getInstance().receiveResponse();
        List<Sektor> sektori = (List<Sektor>) response.getResult();
        if (response.getException() == null) {
            return sektori;
        } else {
            throw response.getException();
        }
    }

    public List<ClanOrganizacije> getAllClanovi() throws Exception {
        Request request = new Request();
        request.setOperation(Operation.GET_ALL_CLANOVI);

        Communication.getInstance().sendRequest(request);

        Response response = Communication.getInstance().receiveResponse();
        List<ClanOrganizacije> clanovi;
        if (response.getResult() != null) {
            clanovi = (List<ClanOrganizacije>) response.getResult();
            return clanovi;
        }
        throw new Exception(response.getException().getMessage());
    }

    public void addClan(ClanOrganizacije clan) throws Exception {
        Request request = new Request(Operation.ADD_CLAN, clan);
        Communication.getInstance().sendRequest(request);
        Response responseObject = Communication.getInstance().receiveResponse();
        if (responseObject.getResult() == null) {
            throw new Exception();
        }
    }

    public void deleteClan(ClanOrganizacije clan) throws Exception {
        Request request = new Request(Operation.DELETE_CLAN, clan);
        Communication.getInstance().sendRequest(request);
        Response response = Communication.getInstance().receiveResponse();
        if (response.getResult() == null) {
            throw new Exception(response.getException().getMessage());
        }
    }

    public ClanOrganizacije updateClan(ClanOrganizacije izmenjenClan) throws Exception {
        Request request = new Request(Operation.UPDATE_CLAN, izmenjenClan);
        List<ClanOrganizacije> clanovi = getAllClanovi();

        for (ClanOrganizacije clan : clanovi) {
            if (clan.getClanID() != izmenjenClan.getClanID() && clan.getImePrezime().equals(izmenjenClan.getImePrezime()) && clan.getKontakt().equals(izmenjenClan.getKontakt())) {
                ClanOrganizacije c = new ClanOrganizacije();
                c.setClanID(-1001);
                return c;
            }
        }

        Communication.getInstance().sendRequest(request);

        Response response = Communication.getInstance().receiveResponse();
        if (response.getResult() != null) {
            ClanOrganizacije noviClan = (ClanOrganizacije) response.getResult();
            return noviClan;
        }
        throw new Exception(response.getException().getMessage());
    }

    public List<Projekat> getAllProjekti() throws Exception {
        Request request = new Request();
        request.setOperation(Operation.GET_ALL_PROJEKTI);

        Communication.getInstance().sendRequest(request);

        Response response = Communication.getInstance().receiveResponse();
        List<Projekat> projekti;
        if (response.getResult() != null) {
            projekti = (List<Projekat>) response.getResult();
            return projekti;
        }
        throw new Exception(response.getException().getMessage());
    }

    public void deleteProjekat(Projekat projekat) throws Exception {
        Request request = new Request(Operation.DELETE_PROJEKAT, projekat);
        Communication.getInstance().sendRequest(request);
        Response response = Communication.getInstance().receiveResponse();
        if (response.getResult() == null) {
            throw new Exception("Dogodila se greska u komunikaciji!(server je ugasen)");
        }
    }

    public List<Aktivnost> getAllAktivnostiProjekta(Projekat projekat) throws Exception {
        Request request = new Request();
        request.setOperation(Operation.GET_ALL_AKTIVNOSTI_PROJEKTA);
        request.setArgument(projekat);
        Communication.getInstance().sendRequest(request);

        Response response = Communication.getInstance().receiveResponse();
        List<Aktivnost> aktivnosti;
        if (response.getResult() != null) {
            aktivnosti = (List<Aktivnost>) response.getResult();
            return aktivnosti;
        }
        throw new Exception(response.getException().getMessage());
    }

    public void deleteAktivnost(Aktivnost aktivnost) throws Exception {
        Request request = new Request(Operation.DELETE_AKTIVNOST, aktivnost);
        Communication.getInstance().sendRequest(request);
        Response response = Communication.getInstance().receiveResponse();
         if (response.getResult() == null) {
            throw new Exception(response.getException().getMessage());
        }
    }

    public List<Zadatak> getAllZadaciAktivnostiProjekta(Aktivnost aktivnost) throws Exception {
        Request request = new Request();
        request.setOperation(Operation.GET_ALL_ZADACI_AKTIVNOSTI_PROJEKTA);
        request.setArgument(aktivnost);
        Communication.getInstance().sendRequest(request);

        Response response = Communication.getInstance().receiveResponse();
        List<Zadatak> zadaci;
        if (response.getResult() != null) {
            zadaci = (List<Zadatak>) response.getResult();
            return zadaci;
        }
        throw new Exception(response.getException().getMessage());
    }

    public void addProjekat(Projekat projekat) throws Exception {
        Request request = new Request(Operation.ADD_PROJEKAT, projekat);
        Communication.getInstance().sendRequest(request);
        Response responseObject = Communication.getInstance().receiveResponse();
        if (responseObject.getResult() == null) {
            throw new Exception(responseObject.getException().getMessage());
        }
    }

    public Projekat updateProjekat(Projekat proj, Projekat izmenjenProjekat) throws Exception {
        Map<String, Projekat> data = new HashMap<>();
        data.put("stari", proj);
        data.put("novi", izmenjenProjekat);

        Request request = new Request(Operation.UPDATE_PROJEKAT, data);
        List<Projekat> projekti = getAllProjekti();

        Communication.getInstance().sendRequest(request);

        Response response = Communication.getInstance().receiveResponse();
        if (response.getResult() != null) {
            Projekat noviProjekat = (Projekat) response.getResult();
            return noviProjekat;
        }
        throw new Exception(response.getException().getMessage());

    }

    public Sektor updateSektor(Sektor s, ClanOrganizacije cl) throws Exception {
        Map<String, IDomainObject> data = new HashMap<>();
        
        data.put("sektor", s);
        data.put("clan", cl);
        Request request = new Request(Operation.UPDATE_SEKTOR, data);
        Communication.getInstance().sendRequest(request);

        Response response = Communication.getInstance().receiveResponse();
        if (response.getResult() != null) {
            Sektor sekt = (Sektor) response.getResult();
            return sekt;
        }
        throw new Exception(response.getException().getMessage());
    }

}
