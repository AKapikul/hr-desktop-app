/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs.ac.bg.fon.ps.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import rs.ac.bg.fon.ps.communication.Communication;
import rs.ac.bg.fon.ps.domain.ClanOrganizacije;
import rs.ac.bg.fon.ps.domain.Projekat;
import rs.ac.bg.fon.ps.domain.Sektor;
import rs.ac.bg.fon.ps.domain.User;
import rs.ac.bg.fon.ps.domain.enumeration.FrmMode;
import rs.ac.bg.fon.ps.view.form.FrmClan;
import rs.ac.bg.fon.ps.view.form.components.table.ClanTableModel;

/**
 *
 * @author Anja
 */
public class FrmClanController {

    private final User currentUser;
    FrmClan frmClan;
    SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
    private ClanTableModel ctm;
    private ClanOrganizacije cl;

    public FrmClan getFrmClan() {
        return frmClan;
    }
    
    

    public FrmClanController(User user) {
        currentUser = user;
    }

    public ClanOrganizacije getCl() {
        return cl;
    }

    public void setCl(ClanOrganizacije cl) {
        this.cl = cl;
    }

    void openFrmClan(ClanOrganizacije clan, FrmMode frmMode, ClanTableModel ctm) throws Exception {
        frmClan = new FrmClan();
        frmClan.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.ctm = ctm;
        addListenersFrmClan();
        prepareView(frmMode, clan);
        frmClan.setVisible(true);

    }

    private void addListenersFrmClan() {
        frmClan.addBtnDodajClanaActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Sektor s = (Sektor) frmClan.getCbSektor().getSelectedItem();
                String imePrezime = frmClan.getTxtImePrezime().getText();

                if (imePrezime.isEmpty()) {
                    JOptionPane.showMessageDialog(frmClan, "Morate uneti ime i prezime!", "Dodavanje novog člana", JOptionPane.ERROR_MESSAGE);
                }

                Date datumUclanjenja = null;
                Date datumIsclanjenja = null;

                try {
                    datumUclanjenja = sdf.parse(frmClan.getTxtDatumUclanjenja().getText());
                } catch (ParseException ex) {
                    JOptionPane.showMessageDialog(frmClan, "Datum učlanjenja mora biti u formatu dd.MM.yyyy!", "Dodavanje novog člana", JOptionPane.ERROR_MESSAGE);
                }
                if (!frmClan.getTxtDatumIsclanjenja().getText().isEmpty()) {
                    try {
                        datumIsclanjenja = sdf.parse(frmClan.getTxtDatumIsclanjenja().getText());
                    } catch (ParseException ex) {
                        JOptionPane.showMessageDialog(frmClan, "Datum iščlanjenja mora biti u formatu dd.MM.yyyy!", "Dodavanje novog člana", JOptionPane.ERROR_MESSAGE);
                    }
                }
                String kontakt = frmClan.getTxtKontakt().getText();

                ClanOrganizacije clan = new ClanOrganizacije(-1, imePrezime, datumUclanjenja, datumIsclanjenja, s, kontakt, 0);
                try {
                    Communication.getInstance().addClan(clan);

                    // ctm.addClan(clan);
                    List<ClanOrganizacije> clanovi = getListClanovi();
                    ctm.update(clanovi);                    
                    JOptionPane.showMessageDialog(frmClan, "Sistem je zapamtio člana organizacije!", "Dodavanje novog člana", JOptionPane.INFORMATION_MESSAGE);
                    frmClan.dispose();
                    return;
                } catch (Exception ex) {
                    ex.printStackTrace();
                    JOptionPane.showMessageDialog(frmClan, "Sistem ne može da zapamti člana organizacije!"+ex.getMessage() , "Dodavanje novog člana", JOptionPane.ERROR_MESSAGE);

                }
            }
        });
        frmClan.addBtnOmoguciIzmeneActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try{
                frmClan.getLblIdClana().setEnabled(false);
                frmClan.getTxtImePrezime().setEnabled(true);
                frmClan.getTxtDatumUclanjenja().setEnabled(true);
                frmClan.getTxtDatumIsclanjenja().setEnabled(true);
                frmClan.getTxtKontakt().setEnabled(true);

                frmClan.getBtnDodajClana().setVisible(false);
                frmClan.getBtnIzmeniClana().setVisible(true);
                frmClan.getBtnObrisiClana().setVisible(false);
                frmClan.getBtnOmoguciIzmene().setVisible(false);
                }catch(Exception ex){
                    JOptionPane.showMessageDialog(frmClan, "Dogodila se greska u komunikaciji!(server je ugasen)");
            System.exit(0);
                }
            }
        });
        frmClan.addBtnObrisiClanaActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    for (Sektor sektor : getListSektori()) {
                        if (sektor.getKoordinatorSektoraID() == cl.getClanID()) {
                            JOptionPane.showMessageDialog(frmClan, "Ne mozete obrisati člana " + cl + " jer je koordinator sektora " + sektor + "!", "Brisanje člana", JOptionPane.ERROR_MESSAGE);
                            return;
                        }
                    }
                    for (Projekat projekat : getListProjekti()) {
                        if (projekat.getKoordinatorProjekta().equals(cl)) {
                            JOptionPane.showMessageDialog(frmClan, "Ne mozete obrisati člana " + cl + " jer je koordinator projekta " + projekat + "!", "Brisanje člana", JOptionPane.ERROR_MESSAGE);
                            return;
                        }
                    }
                    int opt = JOptionPane.showConfirmDialog(frmClan, "Da li ste sigurni da želite da izvršite brisanje?", "Brisanje člana", JOptionPane.YES_NO_OPTION);
                        if(opt == JOptionPane.YES_OPTION){
                    Communication.getInstance().deleteClan(cl);
                    //ctm.deleteClan(cl);
                    List<ClanOrganizacije> clanovi = Communication.getInstance().getAllClanovi();
                    ctm.update(clanovi);
                    frmClan.dispose();
                    } else { return;}
                } catch (Exception ex) {
                    ex.printStackTrace();
                    JOptionPane.showMessageDialog(frmClan, "Sistem ne može da obriše čana organizacije"+ex.getMessage(), "Brisanje člana", JOptionPane.ERROR_MESSAGE);
                    return;
                }
                JOptionPane.showMessageDialog(frmClan, "Sistem je obrisao člana organizacije!", "Brisanje člana", JOptionPane.INFORMATION_MESSAGE);

            }

        });
        frmClan.addBtnIzmeniClanaActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    int id = cl.getClanID();
                    String imePr = frmClan.getTxtImePrezime().getText().trim();
                    
                    if (imePr.isEmpty()) {
                        JOptionPane.showMessageDialog(frmClan, "Morate uneti ime i prezime!", "Izmena podataka o članu", JOptionPane.ERROR_MESSAGE);
                    }
                    
                    String kont = frmClan.getTxtKontakt().getText().trim();
                    Date datumU = null;
                    Date datumI = null;
                    try {
                        datumU = sdf.parse(frmClan.getTxtDatumUclanjenja().getText());
                    } catch (ParseException ex) {
                        JOptionPane.showMessageDialog(frmClan, "Datum učlanjenja mora biti u formatu dd.MM.yyyy!", "Izmena podataka o članu", JOptionPane.ERROR_MESSAGE);
                    }
                    if (!frmClan.getTxtDatumIsclanjenja().getText().isEmpty()) {
                        try {
                            datumI = sdf.parse(frmClan.getTxtDatumIsclanjenja().getText());
                        } catch (ParseException ex) {
                            JOptionPane.showMessageDialog(frmClan, "Datum iščlanjenja mora biti u formatu dd.MM.yyyy!", "Izmena podataka o članu", JOptionPane.ERROR_MESSAGE);
                        }
                    }
                    
                    Sektor s = (Sektor) frmClan.getCbSektor().getSelectedItem();
                    
                    ClanOrganizacije izmenjenClan = new ClanOrganizacije(id, imePr, datumU, datumI, s, kont, 0);
                    try {
                        izmenjenClan = Communication.getInstance().updateClan(izmenjenClan);
                        if (izmenjenClan.getClanID() == -1001) {
                            JOptionPane.showMessageDialog(frmClan, "Pokusavate da unesete člana koji već postoji", "Izmena podataka o članu", JOptionPane.ERROR_MESSAGE);
                            return;
                        }
                    } catch (Exception ex) {
                        JOptionPane.showMessageDialog(frmClan, "Sistem ne može da izmeni člana organizacije!"+ex.getMessage(), "Izmena podataka o članu", JOptionPane.ERROR_MESSAGE);
                        System.exit(0);
                    }
                    
                    JOptionPane.showMessageDialog(frmClan, "Sistem je zapamtio člana organizacije!", "Izmena podataka o članu", JOptionPane.INFORMATION_MESSAGE);
                    frmClan.dispose();
                    
                    List<ClanOrganizacije> clanovi = getListClanovi();
                    ctm.update(clanovi);
                    
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(frmClan, ex.getMessage(), "Error",JOptionPane.ERROR_MESSAGE);
                }

            }

        });
    }

    private void prepareView(FrmMode formMode, ClanOrganizacije clan) throws Exception {
        fillCbSektor();
        frmClan.getLblCurrentUser().setText(currentUser.getFirstname() + " " + currentUser.getLastname());

        if (formMode.equals(FrmMode.FORM_ADD)) {
            frmClan.setTitle("Human Resources app - dodaj novog člana organizacije");
            frmClan.getLblIdClana().setVisible(false);
            frmClan.getLblID().setVisible(false);
            frmClan.getTxtImePrezime().setEnabled(true);
            frmClan.getTxtDatumUclanjenja().setEnabled(true);
            frmClan.getTxtDatumIsclanjenja().setEnabled(true);
            frmClan.getTxtKontakt().setEnabled(true);
            frmClan.getLblBrojBodova().setEnabled(false);
            frmClan.getLblBrojBodova().setText("0");

            frmClan.getBtnDodajClana().setVisible(true);
            frmClan.getBtnIzmeniClana().setVisible(false);
            frmClan.getBtnObrisiClana().setVisible(false);
            frmClan.getBtnOmoguciIzmene().setVisible(false);
        }

        if (formMode.equals(FrmMode.FORM_UPDATE)) {
            frmClan.getLblIdClana().setEnabled(false);
            frmClan.getLblBrojBodova().setEnabled(false);
            frmClan.getTxtImePrezime().setEnabled(true);
            frmClan.getTxtDatumUclanjenja().setEnabled(true);
            frmClan.getTxtDatumIsclanjenja().setEnabled(true);
            frmClan.getTxtKontakt().setEnabled(true);

            frmClan.getBtnDodajClana().setVisible(false);
            frmClan.getBtnIzmeniClana().setVisible(true);
            frmClan.getBtnObrisiClana().setVisible(false);
            frmClan.getBtnOmoguciIzmene().setVisible(false);

            frmClan.getLblIdClana().setText(clan.getClanID() + "");
            frmClan.getLblBrojBodova().setText(clan.getBrojBodova() + "");
            frmClan.getTxtImePrezime().setText(clan.getImePrezime());
            frmClan.getTxtDatumUclanjenja().setText(sdf.format(clan.getDatumUclanjenja()));
            if (clan.getDatumIsclanjenja() != null) {
                frmClan.getTxtDatumIsclanjenja().setText(sdf.format(clan.getDatumIsclanjenja()));
            } else {
                frmClan.getTxtDatumIsclanjenja().setText("");
            }
            frmClan.getTxtKontakt().setText(clan.getKontakt());
            frmClan.getCbSektor().setSelectedItem(clan.getSektor());
            setCl(clan);
        }

        if (formMode.equals(FrmMode.FORM_VIEW)) {
            frmClan.getLblIdClana().setEnabled(false);
            frmClan.getLblBrojBodova().setEnabled(false);
            frmClan.getTxtImePrezime().setEnabled(false);
            frmClan.getTxtDatumUclanjenja().setEnabled(false);
            frmClan.getTxtDatumIsclanjenja().setEnabled(false);
            frmClan.getTxtKontakt().setEnabled(false);
            frmClan.getCbSektor().setEnabled(false);

            frmClan.getBtnDodajClana().setVisible(false);
            frmClan.getBtnIzmeniClana().setVisible(false);
            frmClan.getBtnObrisiClana().setVisible(true);
            frmClan.getBtnOmoguciIzmene().setVisible(true);

            frmClan.getLblIdClana().setText(clan.getClanID() + "");
            frmClan.getLblBrojBodova().setText(clan.getBrojBodova() + "");
            frmClan.getTxtImePrezime().setText(clan.getImePrezime());
            frmClan.getTxtDatumUclanjenja().setText(sdf.format(clan.getDatumUclanjenja()));
            if (clan.getDatumIsclanjenja() != null) {
                frmClan.getTxtDatumIsclanjenja().setText(sdf.format(clan.getDatumIsclanjenja()));
            } else {
                frmClan.getTxtDatumIsclanjenja().setText("");
            }
            frmClan.getTxtKontakt().setText(clan.getKontakt());
            frmClan.getCbSektor().setSelectedItem(clan.getSektor());
            setCl(clan);
        }

    }

    private void fillCbSektor() throws Exception {
        frmClan.getCbSektor().removeAllItems();
        List<Sektor> sektori = getListSektori();

        for (Sektor sektor : sektori) {
            frmClan.getCbSektor().addItem(sektor);
        }

    }

    private List<Sektor> getListSektori() throws Exception {
        List<Sektor> sektori = new ArrayList<>();
        try {
            sektori = Communication.getInstance().getAllSektor();
        } catch (Exception ex) {
                   throw new Exception("\nDogodila se greska u komunikaciji!(server je ugasen)");
     
//            System.exit(0);
        }
        return sektori;
    }

    private List<ClanOrganizacije> getListClanovi() throws Exception {
        List<ClanOrganizacije> clanovi = null;
        try {
            clanovi = Communication.getInstance().getAllClanovi();
        } catch (Exception ex) {
                        throw new Exception("\nDogodila se greska u komunikaciji!(server je ugasen)");

            //System.exit(0);
        }
        return clanovi;
    }

    private List<Projekat> getListProjekti() throws Exception {
        List<Projekat> projekti = new ArrayList<>();
        try {
            projekti = Communication.getInstance().getAllProjekti();
        } catch (Exception ex) {
                        throw new Exception("\nDogodila se greska u komunikaciji!(server je ugasen)");

//            System.exit(0);
        }
        return projekti;
    }
}
