/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs.ac.bg.fon.ps.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import rs.ac.bg.fon.ps.communication.Communication;
import rs.ac.bg.fon.ps.domain.ClanOrganizacije;
import rs.ac.bg.fon.ps.domain.Projekat;
import rs.ac.bg.fon.ps.domain.Sektor;
import rs.ac.bg.fon.ps.domain.User;
import rs.ac.bg.fon.ps.domain.enumeration.FrmMode;
import rs.ac.bg.fon.ps.view.form.FrmClan;
import rs.ac.bg.fon.ps.view.form.FrmClanoviPregled;
import rs.ac.bg.fon.ps.view.form.components.table.ClanTableModel;

/**
 *
 * @author Anja
 */
public class FrmClanoviPregledController {

    private final User currentUser;
    FrmClanoviPregled frmClanoviPregled;
    List<ClanOrganizacije> pretraga;

    public FrmClanoviPregledController(User user) {
        currentUser = user;
        pretraga = new ArrayList<>();
    }

    public void openFrmClanoviPregled() throws Exception {
        frmClanoviPregled = new FrmClanoviPregled();
        frmClanoviPregled.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        addListenersFrmClanoviPregled();
        prepareView();
        frmClanoviPregled.setVisible(true);
    }

    private void prepareView() throws Exception {
        frmClanoviPregled.getLblCurrentUser().setText(currentUser.getFirstname() + " " + currentUser.getLastname());
        ClanTableModel ctm = new ClanTableModel(getListClanovi());
        frmClanoviPregled.getTblClanovi().setModel(ctm);
        pretraga = ctm.getClanovi();
    }

    private void addListenersFrmClanoviPregled() {
        frmClanoviPregled.addBtnDodajNovogClanaActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    ClanTableModel ctm = (ClanTableModel) frmClanoviPregled.getTblClanovi().getModel();
                    FormCoordinator.getInstance().openFrmClan(null, FrmMode.FORM_ADD, ctm);
                } catch (Exception ex) {
                    ex.printStackTrace();
                    JOptionPane.showMessageDialog(frmClanoviPregled, "Dogodila se greska u komunikaciji!(server je ugasen)" + ex.getMessage(), "Dodavanje novog člana", JOptionPane.ERROR_MESSAGE);

                }
            }
        });
        frmClanoviPregled.addBtnIzmeniClanaActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                FrmClan frmClan = new FrmClan();
                int selectedRow = frmClanoviPregled.getTblClanovi().getSelectedRow();
                if (selectedRow != -1) {
                    ClanTableModel ctm = (ClanTableModel) frmClanoviPregled.getTblClanovi().getModel();
                    ClanOrganizacije clan = ctm.getClan(selectedRow);
                    System.out.println(clan);
                    try {

                        frmClan = FormCoordinator.getInstance().openFrmClan(clan, FrmMode.FORM_UPDATE, ctm);
                    } catch (Exception ex) {
                        ex.printStackTrace();
                        JOptionPane.showMessageDialog(frmClanoviPregled, "Sistem ne može da učita člana organizacije!", "Izmena člana", JOptionPane.ERROR_MESSAGE);
                        return;
                    }
                } else {
                    JOptionPane.showMessageDialog(frmClanoviPregled, "Morate označiti člana za izmenu!", "Izmena člana organizacije", JOptionPane.ERROR_MESSAGE);
                }
                JOptionPane.showMessageDialog(frmClan, "Sistem je učitao člana organizacije!", "Izmena člana organizacije", JOptionPane.INFORMATION_MESSAGE);

            }
        });
        frmClanoviPregled.addBtnObrisiClanaActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int selectedRow = frmClanoviPregled.getTblClanovi().getSelectedRow();
                if (selectedRow != -1) {
                    ClanTableModel ctm = (ClanTableModel) frmClanoviPregled.getTblClanovi().getModel();
                    ClanOrganizacije clan = ctm.getClan(selectedRow);
                    System.out.println(clan);
                    try {
                        for (Sektor sektor : getListSektori()) {
                            if (sektor.getKoordinatorSektoraID() == clan.getClanID()) {
                                JOptionPane.showMessageDialog(frmClanoviPregled, "Ne mozete obrisati člana " + clan + " jer je koordinator sektora " + sektor + "!", "Brisanje člana", JOptionPane.ERROR_MESSAGE);
                                return;
                            }
                        }
                        for (Projekat projekat : getListProjekti()) {
                            if (projekat.getKoordinatorProjekta().equals(clan)) {
                                JOptionPane.showMessageDialog(frmClanoviPregled, "Ne mozete obrisati člana " + clan + " jer je koordinator projekta " + projekat + "!", "Brisanje člana", JOptionPane.ERROR_MESSAGE);
                                return;
                            }
                        }
                        int opt = JOptionPane.showConfirmDialog(frmClanoviPregled, "Da li ste sigurni da želite da izvršite brisanje?", "Brisanje člana", JOptionPane.YES_NO_OPTION);
                        if (opt == JOptionPane.YES_OPTION) {
                            Communication.getInstance().deleteClan(clan);
                            ctm.deleteClan(selectedRow);
                            List<ClanOrganizacije> clanovi = ctm.getClanovi();
                            ctm.update(clanovi);
                        } else {
                            return;
                        }

                    } catch (Exception ex) {
                        ex.printStackTrace();
                        JOptionPane.showMessageDialog(frmClanoviPregled, "Sitem ne može da obriše člana organizacije!" + ex.getMessage(), "Brisanje člana", JOptionPane.ERROR_MESSAGE);
                        return;
                    }
                    JOptionPane.showMessageDialog(frmClanoviPregled, "Uspešno obrisan član!", "Brisanje člana", JOptionPane.INFORMATION_MESSAGE);
                } else {
                    JOptionPane.showMessageDialog(frmClanoviPregled, "Morate označiti člana za brisanje!", "Brisanje člana organizacije", JOptionPane.ERROR_MESSAGE);

                }
            }
        });
        frmClanoviPregled.addBtnPrikaziDetaljeActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                FrmClan frmClan = new FrmClan();
                int selectedRow = frmClanoviPregled.getTblClanovi().getSelectedRow();
                if (selectedRow != -1) {
                    ClanTableModel ctm = (ClanTableModel) frmClanoviPregled.getTblClanovi().getModel();
                    ClanOrganizacije clan = ctm.getClan(selectedRow);
                    System.out.println(clan);
                    try {
                        frmClan = FormCoordinator.getInstance().openFrmClan(clan, FrmMode.FORM_VIEW, ctm);

                    } catch (Exception ex) {
                        JOptionPane.showMessageDialog(frmClanoviPregled, "Sistem ne može da učita člana organizacije!" + ex.getMessage(), "Pregled člana", JOptionPane.ERROR_MESSAGE);
                        return;
                    }
                } else {
                    JOptionPane.showMessageDialog(frmClanoviPregled, "Morate označiti člana za pregled!", "Pregled člana organizacije", JOptionPane.ERROR_MESSAGE);
                }
                JOptionPane.showMessageDialog(frmClan, "Sistem je učitao člana organizacije!", "Pregled člana", JOptionPane.INFORMATION_MESSAGE);

            }
        });
        frmClanoviPregled.addChbPrikaziAktivneActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    ClanTableModel ctm = (ClanTableModel) frmClanoviPregled.getTblClanovi().getModel();

                    if (frmClanoviPregled.getChbPrikaziAktivne().isSelected()) {
                        List<ClanOrganizacije> aktivni = new ArrayList<>();
                        for (ClanOrganizacije clanOrganizacije : pretraga) {
                            if (clanOrganizacije.getDatumIsclanjenja() == null) {
                                aktivni.add(clanOrganizacije);
                            }
                        }
                        ctm.update(aktivni);
                    }
                    if (!frmClanoviPregled.getChbPrikaziAktivne().isSelected()) {
                        ctm.update(pretraga);
                    }
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(frmClanoviPregled, ex.getMessage(), "Pretraga članova", JOptionPane.ERROR_MESSAGE);
                    return;
                }
            }
        });
        frmClanoviPregled.addBtnPretraziActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    ClanTableModel ctm = (ClanTableModel) frmClanoviPregled.getTblClanovi().getModel();
                    List<ClanOrganizacije> svi = getListClanovi();
                    String kriterijumIme = frmClanoviPregled.getTxtPretragaImePrezime().getText().trim().toLowerCase();
                    String kriterijumSektor = frmClanoviPregled.getTxtPretragaSektor().getText().trim().toLowerCase();
                    List<ClanOrganizacije> pret = new ArrayList<>();
                    for (ClanOrganizacije clanOrganizacije : svi) {
                        if (clanOrganizacije.getImePrezime().toLowerCase().contains(kriterijumIme) && (clanOrganizacije.getSektor().getSifraSektora().toLowerCase().contains(kriterijumSektor) || clanOrganizacije.getSektor().getPunNaziv().toLowerCase().contains(kriterijumSektor))) {
                            pret.add(clanOrganizacije);
                        }
                    }
                    pretraga = pret;
                    frmClanoviPregled.getChbPrikaziAktivne().setSelected(false);

                    ctm.update(pret);
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(frmClanoviPregled, "Sistem ne može da nađe članove organizacije po zadatoj vrednosti!" + ex.getMessage(), "Pretraga članova", JOptionPane.ERROR_MESSAGE);
                    return;
                }
                JOptionPane.showMessageDialog(frmClanoviPregled, "Sistem je našao članove organizacije po zadatoj vrednosti!", "Pretraga članova", JOptionPane.INFORMATION_MESSAGE);

            }
        }
        );

    }

    private List<ClanOrganizacije> getListClanovi() throws Exception {
        List<ClanOrganizacije> clanovi = new ArrayList<>();
        try {
            clanovi = Communication.getInstance().getAllClanovi();
        } catch (Exception ex) {
            throw new Exception("\nDogodila se greska u komunikaciji!(server je ugasen)");
            //  System.exit(0);
        }
        return clanovi;
    }

    private List<Sektor> getListSektori() throws Exception {
        List<Sektor> sektori = new ArrayList<>();
        try {
            sektori = Communication.getInstance().getAllSektor();
        } catch (Exception ex) {
            throw new Exception("\nDogodila se greska u komunikaciji!(server je ugasen)");
            //  System.exit(0);
        }
        return sektori;
    }

    private List<Projekat> getListProjekti() throws Exception {
        List<Projekat> projekti = new ArrayList<>();
        try {
            projekti = Communication.getInstance().getAllProjekti();
        } catch (Exception ex) {
            throw new Exception("\nDogodila se greska u komunikaciji!(server je ugasen)");
            //System.exit(0);
        }
        return projekti;
    }
}
