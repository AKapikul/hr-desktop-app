/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs.ac.bg.fon.ps.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import rs.ac.bg.fon.ps.communication.Communication;
import rs.ac.bg.fon.ps.domain.Projekat;
import rs.ac.bg.fon.ps.domain.User;
import rs.ac.bg.fon.ps.domain.enumeration.FrmMode;
import rs.ac.bg.fon.ps.view.form.FrmProjekat;
import rs.ac.bg.fon.ps.view.form.FrmProjektiPregled;
import rs.ac.bg.fon.ps.view.form.components.table.ProjekatTableModel;

/**
 *
 * @author Anja
 */
public class FrmProjektiPregledController {

    private final User currentUser;
    FrmProjektiPregled frmProjektiPregled;
    List<Projekat> pretraga;

    public FrmProjektiPregledController(User user) {
        currentUser = user;
        pretraga = new ArrayList<>();
    }

    public void openFrmProjektiPregled() throws Exception {
        frmProjektiPregled = new FrmProjektiPregled();
        frmProjektiPregled.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        addListenersFrmProjektiPregled();
        prepareView();
        frmProjektiPregled.setVisible(true);

    }

    private void prepareView() throws Exception {
        frmProjektiPregled.getLblCurrentUser().setText(currentUser.getFirstname() + " " + currentUser.getLastname());
        ProjekatTableModel ptm = new ProjekatTableModel(getListProjekti());
        frmProjektiPregled.getTblProjekti().setModel(ptm);
        pretraga = ptm.getProjekti();
    }

    private void addListenersFrmProjektiPregled() {
        frmProjektiPregled.addBtnDodajNoviProjekatActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    ProjekatTableModel ptm = (ProjekatTableModel) frmProjektiPregled.getTblProjekti().getModel();
                    FormCoordinator.getInstance().openFrmProjekat(new Projekat(), FrmMode.FORM_ADD, ptm);
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(frmProjektiPregled, ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
                    return;
                }
            }
        });
        frmProjektiPregled.addBtnIzmeniProjekatActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                FrmProjekat frmP = new FrmProjekat();
                int selectedRow = frmProjektiPregled.getTblProjekti().getSelectedRow();
                if (selectedRow != -1) {
                    ProjekatTableModel ptm = (ProjekatTableModel) frmProjektiPregled.getTblProjekti().getModel();
                    Projekat projekat = ptm.getProjekat(selectedRow);
                    System.out.println(projekat);
                    try {
                        frmP = FormCoordinator.getInstance().openFrmProjekat(projekat, FrmMode.FORM_UPDATE, ptm);
                    } catch (Exception ex) {
                        JOptionPane.showMessageDialog(frmProjektiPregled, ex.getMessage(), "Izmena projekta", JOptionPane.ERROR_MESSAGE);
                        return;
                    }
                } else {
                    JOptionPane.showMessageDialog(frmProjektiPregled, "Morate označiti projekat za izmenu!", "Izmena projekta", JOptionPane.ERROR_MESSAGE);
                }
                JOptionPane.showMessageDialog(frmP, "Sistem je učitao projekat!", "Izmena projekta", JOptionPane.INFORMATION_MESSAGE);

            }
        }
        );
        frmProjektiPregled.addBtnPrikaziDetaljeActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                FrmProjekat frmP = new FrmProjekat();
                int selectedRow = frmProjektiPregled.getTblProjekti().getSelectedRow();
                if (selectedRow != -1) {
                    ProjekatTableModel ptm = (ProjekatTableModel) frmProjektiPregled.getTblProjekti().getModel();
                    Projekat projekat = ptm.getProjekat(selectedRow);
                    System.out.println(projekat);
                    try {
                        frmP = FormCoordinator.getInstance().openFrmProjekat(projekat, FrmMode.FORM_VIEW, ptm);
                    } catch (Exception ex) {
                        ex.printStackTrace();
                        JOptionPane.showMessageDialog(frmProjektiPregled, "Sistem ne može da učita projekat!" + ex.getMessage(), "Pregled projekta", JOptionPane.ERROR_MESSAGE);
                        return;
                    }
                } else {
                    JOptionPane.showMessageDialog(frmProjektiPregled, "Morate označiti projekat za pregled!", "Pregled projekta", JOptionPane.ERROR_MESSAGE);
                    return;
                }
                JOptionPane.showMessageDialog(frmP, "Sistem je ucitao projekat!", "Pregled projekta", JOptionPane.INFORMATION_MESSAGE);

            }
        });
        frmProjektiPregled.addBtnObrisiProjekatActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int selectedRow = frmProjektiPregled.getTblProjekti().getSelectedRow();
                if (selectedRow != -1) {
                    ProjekatTableModel ptm = (ProjekatTableModel) frmProjektiPregled.getTblProjekti().getModel();
                    Projekat projekat = ptm.getProjekat(selectedRow);
                    System.out.println(projekat);
                    try {
                        int opt = JOptionPane.showConfirmDialog(frmProjektiPregled, "Da li ste sigurni da želite da izvršite brisanje?", "Brisanje projekta", JOptionPane.YES_NO_OPTION);
                        if (opt == JOptionPane.YES_OPTION) {
                            Communication.getInstance().deleteProjekat(projekat);
                            ptm.deleteProjekat(selectedRow);
                            List<Projekat> projekti = ptm.getProjekti();
                            ptm.update(projekti);
                        } else {
                            return;
                        }
                    } catch (Exception ex) {
                        ex.printStackTrace();
                        JOptionPane.showMessageDialog(frmProjektiPregled, "Sistem ne može da obriše projekat!", "Brisanje projekta", JOptionPane.ERROR_MESSAGE);
                        return;
                    }
                } else {
                    JOptionPane.showMessageDialog(frmProjektiPregled, "Morate označiti projekat za brisanje!", "Brisanje projekta", JOptionPane.ERROR_MESSAGE);
                    return;
                }
                JOptionPane.showMessageDialog(frmProjektiPregled, "Sistem je obrisao projekat!", "Brisanje projekta", JOptionPane.INFORMATION_MESSAGE);

            }
        });
        frmProjektiPregled.addBtnPretraziActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    ProjekatTableModel ptm = (ProjekatTableModel) frmProjektiPregled.getTblProjekti().getModel();
                    List<Projekat> svi = getListProjekti();
                    String kriterijumNaziv = frmProjektiPregled.getTxtPretragaNaziv().getText().trim().toLowerCase();
                    String kriterijumGodina = frmProjektiPregled.getTxtPretragaGodina().getText().trim().toLowerCase();
                    List<Projekat> pret = new ArrayList<>();
                    for (Projekat projekat : svi) {
                        if (projekat.getNazivProjekta().toLowerCase().contains(kriterijumNaziv)) {
                            if (!kriterijumGodina.equals("")) {
                                if (projekat.getDatumOdrzavanja().getYear() + 1900 == Integer.parseInt(kriterijumGodina)) {
                                    pret.add(projekat);
                                }
                            } else {
                                pret.add(projekat);
                            }
                        }
                    }
                    // pretraga = pret;
                    frmProjektiPregled.getChbPrikaziOvogodisnje().setSelected(false);
                    ptm.update(pret);
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(frmProjektiPregled, "Sistem ne može da pronađe projekte po zadatoj vrednosti!" + ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
                    return;
                }
                JOptionPane.showMessageDialog(frmProjektiPregled, "Sistem je našao projekte po zadatoj vrednosti!", "Pretraga projekata", JOptionPane.INFORMATION_MESSAGE);

            }
        });

        frmProjektiPregled.addChbPrikaziOvogodisnjeActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    ProjekatTableModel ptm = (ProjekatTableModel) frmProjektiPregled.getTblProjekti().getModel();

                    if (frmProjektiPregled.getChbPrikaziOvogodisnje().isSelected()) {
                        List<Projekat> ovogodisnji = new ArrayList<>();
                        for (Projekat projekat : pretraga) {
                            if (projekat.getDatumOdrzavanja().getYear() + 1900 == new Date().getYear() + 1900) {
                                ovogodisnji.add(projekat);
                            }

                        }
                        ptm.update(ovogodisnji);
                    }
                    if (!frmProjektiPregled.getChbPrikaziOvogodisnje().isSelected()) {
                        ptm.update(pretraga);
                    }
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(frmProjektiPregled, "Sistem ne može izvršiti pretragu!" + ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
                    return;
                }
            }

        });

    }

    private List<Projekat> getListProjekti() throws Exception {
        List<Projekat> projekti = new ArrayList<>();
        try {
            projekti = Communication.getInstance().getAllProjekti();
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new Exception("\nDogodila se greska u komunikaciji!(server je ugasen)");
//            System.exit(0);
        }
        return projekti;
    }
}
