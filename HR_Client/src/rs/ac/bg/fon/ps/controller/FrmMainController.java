/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs.ac.bg.fon.ps.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;
import rs.ac.bg.fon.ps.communication.Communication;
import rs.ac.bg.fon.ps.domain.ClanOrganizacije;
import rs.ac.bg.fon.ps.domain.Projekat;
import rs.ac.bg.fon.ps.domain.User;
import rs.ac.bg.fon.ps.domain.enumeration.FrmMode;
import rs.ac.bg.fon.ps.view.form.FrmMain;
import rs.ac.bg.fon.ps.view.form.components.table.ClanTableModel;
import rs.ac.bg.fon.ps.view.form.components.table.ProjekatTableModel;

/**
 *
 * @author Anja
 */
class FrmMainController {

    private final User currentUser;
    FrmMain frmMain;

    public FrmMainController(User user) {
        currentUser = user;
    }

    void openFrmMain() {
        frmMain = new FrmMain();
        addListenersFrmMain();
        frmMain.setVisible(true);
        prepareView();
    }

    private void addListenersFrmMain() {
        frmMain.addBtnLogOutActionListeners(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    frmMain.dispose();
                    FormCoordinator.getInstance().openFrmLogin();
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(frmMain, "Dogodila se greska u komunikaciji!(server je ugasen)" + ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
                    return;
                }
            }
        });
        frmMain.addMiPregledClanovaOrganizacijeActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    FormCoordinator.getInstance().openFrmClanoviPregled();
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(frmMain, "Dogodila se greska u komunikaciji!(server je ugasen)" + ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
                    return;
                }
            }
        });
        frmMain.addMiDodajNovogClanaOrganizacijeActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    List<ClanOrganizacije> clanovi = new ArrayList<>();
                    try {
                        clanovi = Communication.getInstance().getAllClanovi();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                    FormCoordinator.getInstance().openFrmClan(null, FrmMode.FORM_ADD, new ClanTableModel(clanovi));
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(frmMain, "Dogodila se greska u komunikaciji!(server je ugasen)" + ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
                    return;
                }
            }

        });
        frmMain.addMiPregledProjekataActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    FormCoordinator.getInstance().openFrmProjektiPregled();
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(frmMain, "Dogodila se greska u komunikaciji!(server je ugasen)" + ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
                    return;
                }
            }
        });
        frmMain.addMiDodajNoviProjekatActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    List<Projekat> projekti = new ArrayList<>();
                    try {
                        projekti = Communication.getInstance().getAllProjekti();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                    FormCoordinator.getInstance().openFrmProjekat(new Projekat(), FrmMode.FORM_ADD, new ProjekatTableModel(projekti));
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(frmMain, "Dogodila se greska u komunikaciji!(server je ugasen)" + ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
                    return;
                }
            }
        });
        frmMain.addMiIzmeniSektorActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    FormCoordinator.getInstance().openFrmSektor();
                } catch (Exception ex) {
                    ex.printStackTrace();
                    JOptionPane.showMessageDialog(frmMain, "Dogodila se greska u komunikaciji!(server je ugasen)" + ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
                    return;
                }
            }
        });
    }

    private void prepareView() {
        frmMain.getLblCurrentUser().setText(currentUser.getFirstname() + " " + currentUser.getLastname());
    }



}
