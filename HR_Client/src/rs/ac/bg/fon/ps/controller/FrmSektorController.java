/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs.ac.bg.fon.ps.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import rs.ac.bg.fon.ps.communication.Communication;
import rs.ac.bg.fon.ps.domain.ClanOrganizacije;
import rs.ac.bg.fon.ps.domain.Sektor;
import rs.ac.bg.fon.ps.domain.User;
import rs.ac.bg.fon.ps.view.form.FrmSektor;

/**
 *
 * @author Anja
 */
public class FrmSektorController {

    private final User currentUser;
    FrmSektor frmSektor;

    public FrmSektorController(User user) {
        currentUser = user;
    }

    void openFrmSektor() {
        frmSektor = new FrmSektor();
        frmSektor.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        prepareView();
        addListenersFrmSektor();
        frmSektor.setVisible(true);
    }

    private void addListenersFrmSektor() {
        frmSektor.addCbSektorActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                fillCbKoordinator();
                Sektor s = (Sektor) frmSektor.getCbSektor().getSelectedItem();
                for (Sektor sektor : getListSektori()) {
                    if (sektor.getSifraSektora().equals(s.getSifraSektora())) {
                        frmSektor.getCbKoorinator().setSelectedItem(s.getKoordinatorSektoraID());
                    }
                }
                for (ClanOrganizacije clanOrganizacije : getListClanovi()) {
                    if (!clanOrganizacije.getSektor().equals(s)) {
                        frmSektor.getCbKoorinator().removeItem(clanOrganizacije);
                    }
                }
            }
        });
        frmSektor.addBtnSaveActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Sektor s = (Sektor) frmSektor.getCbSektor().getSelectedItem();
                ClanOrganizacije cl = (ClanOrganizacije) frmSektor.getCbKoorinator().getSelectedItem();
                try {
                    s = Communication.getInstance().updateSektor(s, cl);
//                    for (Sektor sektor : getListSektori()) {
//                        if (sektor.getKoordinatorSektoraID() == cl.getClanID()) {
//                            JOptionPane.showMessageDialog(frmSektor, "Clan je vec clan drugog sektora!", "Error", JOptionPane.ERROR_MESSAGE);
//                            return;
//                        }
//                    }
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(frmSektor, "Dogodila se greska u komunikaciji!(server je ugasen)", "Izmena podataka o članu", JOptionPane.ERROR_MESSAGE);
                    System.exit(0);
                }
                JOptionPane.showMessageDialog(frmSektor, "Uspesno izmenjen koordinator!","Izmena", JOptionPane.INFORMATION_MESSAGE);
            }
        });
    }

    private void prepareView() {
        frmSektor.getLblCurrentUser().setText(currentUser.getFirstname() + " " + currentUser.getLastname());
        fillCbSektor();
        Sektor s = (Sektor) frmSektor.getCbSektor().getSelectedItem();
        fillCbKoordinator();
        for (ClanOrganizacije clanOrganizacije : getListClanovi()) {
            if (!clanOrganizacije.getSektor().equals(s)) {
                frmSektor.getCbKoorinator().removeItem(clanOrganizacije);
            }
        }
        frmSektor.getCbKoorinator().setSelectedItem(s.getKoordinatorSektoraID());
    }

    private void fillCbSektor() {
        frmSektor.getCbSektor().removeAllItems();
        List<Sektor> sektori = getListSektori();

        for (Sektor sektor : sektori) {
            frmSektor.getCbSektor().addItem(sektor);
        }

    }

    private List<Sektor> getListSektori() {
        List<Sektor> sektori = new ArrayList<>();
        try {
            sektori = Communication.getInstance().getAllSektor();
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(frmSektor, "Dogodila se greska u komunikaciji!(server je ugasen)");
            System.exit(0);
        }
        return sektori;
    }

    private void fillCbKoordinator() {
        frmSektor.getCbKoorinator().removeAllItems();
        List<ClanOrganizacije> clanovi = getListClanovi();
        for (ClanOrganizacije clanOrganizacije : clanovi) {
            frmSektor.getCbKoorinator().addItem(clanOrganizacije);
        }
    }

    private List<ClanOrganizacije> getListClanovi() {
        List<ClanOrganizacije> clanovi = null;
        try {
            clanovi = Communication.getInstance().getAllClanovi();
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(frmSektor, "Dogodila se greska u komunikaciji!(server je ugasen)");
            System.exit(0);
        }
        return clanovi;
    }
}
