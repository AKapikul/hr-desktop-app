/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs.ac.bg.fon.ps.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import javax.swing.JOptionPane;
import rs.ac.bg.fon.ps.communication.Communication;
import rs.ac.bg.fon.ps.domain.User;
import rs.ac.bg.fon.ps.view.form.FrmLogin;

/**
 *
 * @author Anja
 */
public class FrmLoginController {

    private FrmLogin frmLogin;

    public FrmLoginController() {

    }

    void openFrmLogin() {
        frmLogin = new FrmLogin();
        addListenersFrmLogin();
        frmLogin.setVisible(true);
    }

    private void addListenersFrmLogin() {
        frmLogin.addBtnLoginActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    String username = frmLogin.getTxtUsername().getText().trim();
                    String password = String.copyValueOf(frmLogin.getTxtPassword().getPassword()).trim();

                    validateForm(username, password);
                    Communication.getInstance().connect();
                    User user = new User();
                    user.setUsername(username);
                    user.setPassword(password);
                    User user1 = Communication.getInstance().login(user);
                    System.out.println(user1);
                    if (user1 == null) {
                        JOptionPane.showMessageDialog(frmLogin, "Korisnicko ime ili lozinka nisu ispravni!");
                        return;
                    }
                    FormCoordinator.getInstance().setCurrentUser(user1);
                    FormCoordinator.getInstance().openFrmMain();
                    frmLogin.dispose();
                    JOptionPane.showMessageDialog(
                            frmLogin,
                            "Dobrodosli, " + user1.getFirstname() + " " + user1.getLastname(),
                            "Login", JOptionPane.INFORMATION_MESSAGE
                    );                   
                } catch (Exception ex) {
                    if (ex instanceof IOException) {
                        JOptionPane.showMessageDialog(frmLogin, "Dogodila se greska u komunikaciji!(server je ugasen)", "Login", JOptionPane.ERROR_MESSAGE);
                        System.exit(0);
                    } else if (ex instanceof Exception) {
                        JOptionPane.showMessageDialog(frmLogin, ex.getMessage(), "Login", JOptionPane.ERROR_MESSAGE);
                    }
                }
            }

        });

        frmLogin.addBtnCancelActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.exit(0);
            }
        });

    }

    private void validateForm(String username, String password) throws Exception {
        frmLogin.getLblErrUsername().setText("");
        frmLogin.getLblErrPassword().setText("");

        StringBuilder errorMessage = new StringBuilder();
        if (username.isEmpty()) {
            frmLogin.getLblErrUsername().setText("Morate uneti korisnicko ime!");
            errorMessage.append("Morate uneti korisnicko ime!\n");
        }
        if (password.isEmpty()) {
            frmLogin.getLblErrPassword().setText("Morate uneti lozinku!");
            errorMessage.append("Morate uneti lozinku!\n");
        }
        if (!errorMessage.toString().isEmpty()) {
            throw new Exception(errorMessage.toString());
        }
    }

    public FrmLogin getFrmLogin() {
        return frmLogin;
    }

}
