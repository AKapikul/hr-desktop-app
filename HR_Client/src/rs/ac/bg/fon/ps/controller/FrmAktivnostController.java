/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs.ac.bg.fon.ps.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import rs.ac.bg.fon.ps.communication.Communication;
import rs.ac.bg.fon.ps.domain.Aktivnost;
import rs.ac.bg.fon.ps.domain.ClanOrganizacije;
import rs.ac.bg.fon.ps.domain.Projekat;
import rs.ac.bg.fon.ps.domain.User;
import rs.ac.bg.fon.ps.domain.Zadatak;
import rs.ac.bg.fon.ps.domain.enumeration.FrmMode;
import rs.ac.bg.fon.ps.view.form.FrmAktivnost;
import rs.ac.bg.fon.ps.view.form.components.table.AktivnostTableModel;
import rs.ac.bg.fon.ps.view.form.components.table.ZadatakTableModel;

/**
 *
 * @author Anja
 */
public class FrmAktivnostController {

    private User currentUser;
    FrmAktivnost frmAktivnost;
    private AktivnostTableModel atm;
    private Aktivnost a;
    private Zadatak zaIzmenu = null;
    private Projekat p;

    public FrmAktivnostController(User user) {
        currentUser = user;

    }

    public Aktivnost getA() {
        return a;
    }

    public void setA(Aktivnost a) {
        this.a = a;
    }

    void openFrmAktivnost(Aktivnost aktivnost, FrmMode frmMode, AktivnostTableModel atm) throws Exception {

            frmAktivnost = new FrmAktivnost();
            frmAktivnost.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
            this.atm = atm;
            this.a = aktivnost;
            this.p = aktivnost.getProjekat();
            addListenersFrmAktivnost();
            prepareView(frmMode, aktivnost);
            frmAktivnost.setVisible(true);
    }

    private void addListenersFrmAktivnost() {
        frmAktivnost.addBtnObrisiZadatakActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int selectedRow = frmAktivnost.getTblZadaci().getSelectedRow();
                if (selectedRow != -1) {
                    ZadatakTableModel ztm = (ZadatakTableModel) frmAktivnost.getTblZadaci().getModel();
                    Zadatak zadatak = ztm.getZadatak(selectedRow);
                    System.out.println(zadatak);
                    try {
                        int opt = JOptionPane.showConfirmDialog(frmAktivnost, "Da li ste sigurni da želite da izvršite brisanje?", "Brisanje zadatka", JOptionPane.YES_NO_OPTION);
                        if (opt == JOptionPane.YES_OPTION) {
                            // Communication.getInstance().deleteZad(zad); NE SAD U BAZI
                            ztm.deleteZadatak(selectedRow);
                            List<Zadatak> zadaci = ztm.getZadaci();
                            ztm.update(zadaci);
                        } else {
                            return;
                        }
                    } catch (Exception ex) {
                        ex.printStackTrace();
                        JOptionPane.showMessageDialog(frmAktivnost, ex.getMessage(), "Brisanje zadatka", JOptionPane.ERROR_MESSAGE);
                        return;
                    }
                    //  JOptionPane.showMessageDialog(frmAktivnost, "Uspešno obrisan zadatak!", "Brisanje zadatka", JOptionPane.INFORMATION_MESSAGE);
                } else {
                    JOptionPane.showMessageDialog(frmAktivnost, "Morate označiti zadatak za brisanje!", "Brisanje zadatka", JOptionPane.ERROR_MESSAGE);
                }
            }
        });
        frmAktivnost.addBtnIzmeniZadatakActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int selectedRow = frmAktivnost.getTblZadaci().getSelectedRow();
                if (selectedRow != -1) {
                    ZadatakTableModel ztm = (ZadatakTableModel) frmAktivnost.getTblZadaci().getModel();
                    Zadatak zadatak = ztm.getZadatak(selectedRow);
                    System.out.println(zadatak);
                    try {
                        frmAktivnost.getCbIzvrsitelj().setSelectedItem(zadatak.getIzvrsitelj());
                        frmAktivnost.getTxtBrojBodova().setText(zadatak.getBrojBodova() + "");
                        frmAktivnost.getTxtOpisZadatka().setText(zadatak.getOpisZadatka());
                        frmAktivnost.getBtnDodaj().setEnabled(false);
                        frmAktivnost.getBtnIzmeni().setEnabled(true);
                        zaIzmenu = zadatak;
                    } catch (Exception ex) {
                        ex.printStackTrace();
                        JOptionPane.showMessageDialog(frmAktivnost, ex.getMessage(), "Izmena zadatka", JOptionPane.ERROR_MESSAGE);
                        return;
                    }
                } else {
                    JOptionPane.showMessageDialog(frmAktivnost, "Morate označiti zadatak za izmenu!", "Izmena zadatka", JOptionPane.ERROR_MESSAGE);
                }
            }
        });
        frmAktivnost.addBtnDodajNoviZadatakActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    frmAktivnost.getCbIzvrsitelj().setSelectedIndex(0);

                    frmAktivnost.getTxtBrojBodova().setText("");
                    frmAktivnost.getTxtOpisZadatka().setText("");
                    frmAktivnost.getBtnDodaj().setEnabled(true);
                    frmAktivnost.getBtnIzmeni().setEnabled(false);
                } catch (Exception ex) {
                    ex.printStackTrace();
                    JOptionPane.showMessageDialog(frmAktivnost, ex.getMessage(), "Dodavanje zadatka", JOptionPane.ERROR_MESSAGE);
                    return;
                }
            }
        });
        frmAktivnost.addBtnIzmeniActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ClanOrganizacije izvrs = (ClanOrganizacije) frmAktivnost.getCbIzvrsitelj().getSelectedItem();
                String brB = frmAktivnost.getTxtBrojBodova().getText();
                int brojB = 0;
                try {
                    brojB = Integer.parseInt(brB);
                } catch (Exception ex) {
                    //  ex.printStackTrace();
                    JOptionPane.showMessageDialog(frmAktivnost, "Morate uneti broj bodova u numerickom formatu!", "Unos zadatka", JOptionPane.ERROR_MESSAGE);
                    return;
                }
                String opis = frmAktivnost.getTxtOpisZadatka().getText();
                Zadatak z = new Zadatak(a.getProjekat(), a, izvrs, opis, brojB);
                try {
                    ZadatakTableModel ztm = (ZadatakTableModel) frmAktivnost.getTblZadaci().getModel();
                    for (Zadatak zadatak : ztm.getZadaci()) {
                        if (zadatak.getIzvrsitelj().equals(z.getIzvrsitelj())) {
                            JOptionPane.showMessageDialog(frmAktivnost, z.getIzvrsitelj() + " je vec registrovan za rad na drugom zadatku!", "Izmena zadatka", JOptionPane.ERROR_MESSAGE);
                            return;
                        }
                    }
                    ztm.deleteZadatak(zaIzmenu);
                    ztm.addZadatak(z);
                    frmAktivnost.getCbIzvrsitelj().setSelectedIndex(0);
                    frmAktivnost.getTxtBrojBodova().setText("");
                    frmAktivnost.getTxtOpisZadatka().setText("");

                    //JOptionPane.showMessageDialog(frmAktivnost, "Uspešno sačuvan zadatak!", "Dodavanje novog zadatka", JOptionPane.INFORMATION_MESSAGE);
                    //return;
                } catch (Exception ex) {
                    ex.printStackTrace();
                    JOptionPane.showMessageDialog(frmAktivnost, ex.getMessage(), "Dodavanje novog zadatka", JOptionPane.ERROR_MESSAGE);

                }
            }
        });
        frmAktivnost.addBtnDodajActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ClanOrganizacije izvrs = (ClanOrganizacije) frmAktivnost.getCbIzvrsitelj().getSelectedItem();
                String brB = frmAktivnost.getTxtBrojBodova().getText();
                int brojB = 0;
                try {
                    brojB = Integer.parseInt(brB);
                } catch (Exception ex) {
                    //  ex.printStackTrace();
                    JOptionPane.showMessageDialog(frmAktivnost, "Morate uneti broj bodova u numerickom formatu!", "Unos zadatka", JOptionPane.ERROR_MESSAGE);
                    return;
                }
                String opis = frmAktivnost.getTxtOpisZadatka().getText();

                Zadatak z = new Zadatak(a.getProjekat(), a, izvrs, opis, brojB);

                try {
                    ZadatakTableModel ztm = (ZadatakTableModel) frmAktivnost.getTblZadaci().getModel();
                    for (Zadatak zadatak : ztm.getZadaci()) {
                        if (zadatak.getIzvrsitelj().equals(z.getIzvrsitelj())) {
                            JOptionPane.showMessageDialog(frmAktivnost, z.getIzvrsitelj() + " je vec registrovan za rad na drugom zadatku!", "Izmena zadatka", JOptionPane.ERROR_MESSAGE);
                            return;
                        }
                    }
                    //   Communication.getInstance().addZad(zad);

                    // List<Zadatak> zadaci = getListZadaci(a);
                    //  ztm.update(zadaci);
                    ztm.addZadatak(z);
                    frmAktivnost.getCbIzvrsitelj().setSelectedIndex(0);
                    frmAktivnost.getTxtBrojBodova().setText("");
                    frmAktivnost.getTxtOpisZadatka().setText("");

                    //JOptionPane.showMessageDialog(frmAktivnost, "Uspešno sačuvan zadatak!", "Dodavanje novog zadatka", JOptionPane.INFORMATION_MESSAGE);
                    //return;
                } catch (Exception ex) {
                    ex.printStackTrace();
                    JOptionPane.showMessageDialog(frmAktivnost,ex.getMessage(), "Dodavanje novog zadatka", JOptionPane.ERROR_MESSAGE);

                }
            }
        });
        frmAktivnost.addBtnOmoguciIzmeneActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    frmAktivnost.getTxtSifraAktivnosti().setEnabled(true);
                    frmAktivnost.getTxtNazivAktivnosti().setEnabled(true);
                    frmAktivnost.getTxtOpisAktivnosti().setEnabled(true);
                    frmAktivnost.getBtnOmoguciIzmene().setVisible(true);
                    frmAktivnost.getBtnSacuvajIzmene().setVisible(true);
                    frmAktivnost.getBtnObrisiZadatak().setVisible(true);
                    frmAktivnost.getBtnDodajNoviZadatak().setVisible(true);
                    frmAktivnost.getBtnIzmeniZadatak().setVisible(true);
                    frmAktivnost.getPnlNoviZadatak().setVisible(true);
                    frmAktivnost.getBtnObrisiAktivnost().setVisible(false);
                    frmAktivnost.getBtnDodajAktivnost().setVisible(false);
                    frmAktivnost.getBtnOmoguciIzmene().setVisible(false);
                    frmAktivnost.getBtnObrisiAktivnost().setVisible(true);
                    frmAktivnost.getBtnIzmeni().setEnabled(false);

                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(frmAktivnost, "Dogodila se greska u komunikaciji!(server je ugasen)" + ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
                    return;
                }
            }
        });
        frmAktivnost.addBtnSacuvajIzmeneActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    validate();
                    String sifraA = frmAktivnost.getTxtSifraAktivnosti().getText().trim();
                    String nazivA = frmAktivnost.getTxtNazivAktivnosti().getText().trim();
                    String opisA = frmAktivnost.getTxtOpisAktivnosti().getText();
                    List<Zadatak> zadaci = new ArrayList<>();
                    ZadatakTableModel ztm = (ZadatakTableModel) frmAktivnost.getTblZadaci().getModel();
                    for (Zadatak zadatak : ztm.getZadaci()) {
                        zadaci.add(zadatak);
                    }

                    Aktivnost ak = new Aktivnost(p, sifraA, nazivA, opisA);
                    ak.setZadaci(zadaci);
                    atm.deleteAktivnost(a);
                    atm.addAktivnost(ak);
                    frmAktivnost.dispose();
                } catch (Exception ex) {
                    ex.printStackTrace();
                    JOptionPane.showMessageDialog(frmAktivnost, ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
                    return;
                }
            }
        }
        );
        frmAktivnost.addBtnObrisiAktivnostActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    atm.deleteAktivnost(a);
                    frmAktivnost.dispose();
                } catch (Exception ex) {
                    ex.printStackTrace();
                    JOptionPane.showMessageDialog(frmAktivnost, ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);

                }
            }
        });
        frmAktivnost.addBtnDodajAktivnostActionListener(
                new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e
            ) {
                try {
                    validate();
                    String sifraA = frmAktivnost.getTxtSifraAktivnosti().getText().trim();
                    String nazivA = frmAktivnost.getTxtNazivAktivnosti().getText().trim();
                    String opisA = frmAktivnost.getTxtOpisAktivnosti().getText();
                    List<Zadatak> zadaci = new ArrayList<>();
                    ZadatakTableModel ztm = (ZadatakTableModel) frmAktivnost.getTblZadaci().getModel();
                    for (Zadatak zadatak : ztm.getZadaci()) {
                        zadaci.add(zadatak);
                    }

                    Aktivnost a = new Aktivnost(p, sifraA, nazivA, opisA);
                    a.setZadaci(zadaci);
                    atm.addAktivnost(a);
                    frmAktivnost.dispose();
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(frmAktivnost, ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
                    return;
                }
            }
        }
        );
    }

    private void prepareView(FrmMode frmMode, Aktivnost aktivnost) throws Exception {
        frmAktivnost.getLblCurrentUser().setText(currentUser.getFirstname() + " " + currentUser.getLastname());
        frmAktivnost.getLblProjekat().setText(p.getNazivProjekta());
        frmAktivnost.getLblAktivnost().setText(a.getNazivAktivnosti());
        fillCbZadaci();

        if (frmMode.equals(FrmMode.FORM_ADD)) {
            ZadatakTableModel ztm = new ZadatakTableModel(new ArrayList<>());
            frmAktivnost.getTblZadaci().setModel(ztm);

            frmAktivnost.setTitle("Human Resources app - dodaj novu aktivnost");
            frmAktivnost.getBtnOmoguciIzmene().setVisible(false);
            frmAktivnost.getBtnSacuvajIzmene().setVisible(false);
            frmAktivnost.getBtnObrisiAktivnost().setVisible(false);
            frmAktivnost.getBtnIzmeni().setEnabled(false);

        }
        if (frmMode.equals(FrmMode.FORM_VIEW)) {
            ZadatakTableModel ztm = new ZadatakTableModel(getListZadaci(aktivnost));
            frmAktivnost.getTblZadaci().setModel(ztm);

            frmAktivnost.getTxtSifraAktivnosti().setEnabled(false);
            frmAktivnost.getTxtNazivAktivnosti().setEnabled(false);
            frmAktivnost.getTxtOpisAktivnosti().setEnabled(false);
            frmAktivnost.getBtnOmoguciIzmene().setEnabled(true);
            frmAktivnost.getBtnSacuvajIzmene().setVisible(false);
            frmAktivnost.getBtnObrisiZadatak().setVisible(false);
            frmAktivnost.getBtnDodajNoviZadatak().setVisible(false);
            frmAktivnost.getBtnIzmeniZadatak().setVisible(false);
            frmAktivnost.getPnlNoviZadatak().setVisible(false);
            frmAktivnost.getBtnObrisiAktivnost().setVisible(false);
            frmAktivnost.getBtnDodajAktivnost().setVisible(false);

            frmAktivnost.getTxtSifraAktivnosti().setText(a.getSifraAktivnosti());
            frmAktivnost.getTxtNazivAktivnosti().setText(a.getNazivAktivnosti());
            frmAktivnost.getTxtOpisAktivnosti().setText(a.getOpisAktivnosti());

        }
        if (frmMode.equals(FrmMode.FORM_UPDATE)) {
            ZadatakTableModel ztm = new ZadatakTableModel(getListZadaci(aktivnost));
            frmAktivnost.getTblZadaci().setModel(ztm);

            frmAktivnost.getTxtSifraAktivnosti().setEnabled(true);
            frmAktivnost.getTxtNazivAktivnosti().setEnabled(true);
            frmAktivnost.getTxtOpisAktivnosti().setEnabled(true);
            frmAktivnost.getBtnOmoguciIzmene().setVisible(true);
            frmAktivnost.getBtnSacuvajIzmene().setVisible(true);
            frmAktivnost.getBtnObrisiZadatak().setVisible(true);
            frmAktivnost.getBtnDodajNoviZadatak().setVisible(true);
            frmAktivnost.getBtnIzmeniZadatak().setVisible(true);
            frmAktivnost.getPnlNoviZadatak().setVisible(true);
            frmAktivnost.getBtnObrisiAktivnost().setVisible(false);
            frmAktivnost.getBtnDodajAktivnost().setVisible(false);
            frmAktivnost.getBtnObrisiAktivnost().setVisible(true);
            frmAktivnost.getBtnIzmeni().setEnabled(false);

            frmAktivnost.getTxtSifraAktivnosti().setText(a.getSifraAktivnosti());
            frmAktivnost.getTxtNazivAktivnosti().setText(a.getNazivAktivnosti());
            frmAktivnost.getTxtOpisAktivnosti().setText(a.getOpisAktivnosti());

        }
    }

    private void fillCbZadaci() throws Exception {
        frmAktivnost.getCbIzvrsitelj().removeAllItems();
        List<ClanOrganizacije> clanovi = getListClanovi();

        for (ClanOrganizacije clanOrganizacije : clanovi) {
            frmAktivnost.getCbIzvrsitelj().addItem(clanOrganizacije);
        }
    }

    private List<ClanOrganizacije> getListClanovi() throws Exception {
        List<ClanOrganizacije> clanovi = new ArrayList<>();
        try {
            clanovi = Communication.getInstance().getAllClanovi();
        } catch (Exception ex) {
            throw new Exception("\nDogodila se greska u komunikaciji!(server je ugasen)");

            //   System.exit(0);
        }
        return clanovi;
    }

    private List<Zadatak> getListZadaci(Aktivnost aktivnost) throws Exception {
        List<Zadatak> zadaci = new ArrayList<>();
        int sig = 0;
        for (Aktivnost aktivnost1 : getListAktivnost(p)) {
            if (aktivnost1.equals(aktivnost)) {
                sig = 1;
            }
        }
        if (sig == 1) {
            try {
                zadaci = Communication.getInstance().getAllZadaciAktivnostiProjekta(aktivnost);
            } catch (Exception ex) {
                ex.printStackTrace();
                throw new Exception("\nDogodila se greska u komunikaciji!(server je ugasen)");

                //  System.exit(0);
            }
        } else {
            zadaci = aktivnost.getZadaci();
        }
        return zadaci;
    }

    private List<Aktivnost> getListAktivnost(Projekat projekat) throws Exception {
        List<Aktivnost> aktivnosti = new ArrayList<>();
        try {
            aktivnosti = Communication.getInstance().getAllAktivnostiProjekta(projekat);
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new Exception("\nDogodila se greska u komunikaciji!(server je ugasen)");
            //   System.exit(0);
        }
        return aktivnosti;
    }

    private void validate() throws Exception {
        StringBuilder sbErr = new StringBuilder();
        if (frmAktivnost.getTxtSifraAktivnosti().getText().trim() == null) {
            sbErr.append("Morate uneti šifru aktivnosti!\n");
        }
        if (frmAktivnost.getTxtNazivAktivnosti().getText().trim() == null) {
            sbErr.append("Morate uneti naziv aktivnosti!\n");
        }
        if (!sbErr.toString().isEmpty()) {
            throw new Exception(sbErr.toString());
        }
    }
}
