/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs.ac.bg.fon.ps.controller;

import rs.ac.bg.fon.ps.domain.Aktivnost;
import rs.ac.bg.fon.ps.domain.ClanOrganizacije;
import rs.ac.bg.fon.ps.domain.Projekat;
import rs.ac.bg.fon.ps.domain.User;
import rs.ac.bg.fon.ps.domain.enumeration.FrmMode;
import rs.ac.bg.fon.ps.view.form.FrmClan;
import rs.ac.bg.fon.ps.view.form.FrmProjekat;
import rs.ac.bg.fon.ps.view.form.components.table.AktivnostTableModel;
import rs.ac.bg.fon.ps.view.form.components.table.ClanTableModel;
import rs.ac.bg.fon.ps.view.form.components.table.ProjekatTableModel;

/**
 *
 * @author Anja
 */
public class FormCoordinator {

    private static FormCoordinator instance;
    private User currentUser;

    private FrmLoginController frmLoginController;
    private FrmMainController frmMainController;
    private FrmClanoviPregledController frmClanoviPregledController;
    private FrmClanController frmClanController;
    private FrmProjektiPregledController frmProjektiPregledController;
    private FrmProjekatController frmProjekatController;
    private FrmAktivnostController frmAktivnostController;
    private FrmSektorController frmSektorController;

    public static FormCoordinator getInstance() {
        if (instance == null) {
            instance = new FormCoordinator();
        }
        return instance;
    }

    public FormCoordinator() {

    }

    public void openFrmLogin() {
        if (frmLoginController == null) {
            frmLoginController = new FrmLoginController();
        }
        frmLoginController.openFrmLogin();
    }

    public void openFrmMain() {
        if (frmMainController == null) {
            frmMainController = new FrmMainController(currentUser);
        }
        frmMainController.openFrmMain();
    }

    void setCurrentUser(User user) {
        currentUser = user;
    }

    public void openFrmClanoviPregled() throws Exception {
        if (frmClanoviPregledController == null) {
            frmClanoviPregledController = new FrmClanoviPregledController(currentUser);
        }
        frmClanoviPregledController.openFrmClanoviPregled();
    }

    public FrmClan openFrmClan(ClanOrganizacije clan, FrmMode frmMode, ClanTableModel ctm) throws Exception {
        if (frmClanController == null) {
            frmClanController = new FrmClanController(currentUser);
        }
        frmClanController.openFrmClan(clan,frmMode,ctm);
        return frmClanController.getFrmClan();
    }
    
    public void openFrmProjektiPregled() throws Exception {
        if (frmProjektiPregledController == null) {
            frmProjektiPregledController = new FrmProjektiPregledController(currentUser);
        }
        frmProjektiPregledController.openFrmProjektiPregled();
    }
    
    public FrmProjekat openFrmProjekat(Projekat projekat, FrmMode frmMode, ProjekatTableModel ptm) throws Exception {
        if (frmProjekatController == null) {
            frmProjekatController = new FrmProjekatController(currentUser);
        }
        frmProjekatController.openFrmProjekat(projekat,frmMode,ptm);
        return frmProjekatController.getFrmProjekat();
    }
    
    public void openFrmAktivnost(Aktivnost aktivnost, FrmMode frmMode, AktivnostTableModel atm) throws Exception {
        if (frmAktivnostController == null) {
            frmAktivnostController = new FrmAktivnostController(currentUser);
        }
        frmAktivnostController.openFrmAktivnost(aktivnost, frmMode, atm);
    }

    void openFrmSektor() {
         if (frmSektorController == null) {
            frmSektorController = new FrmSektorController(currentUser);
        }
        frmSektorController.openFrmSektor();
    }

    

    
    
    
}
