/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs.ac.bg.fon.ps.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import rs.ac.bg.fon.ps.communication.Communication;
import rs.ac.bg.fon.ps.domain.Aktivnost;
import rs.ac.bg.fon.ps.domain.ClanOrganizacije;
import rs.ac.bg.fon.ps.domain.Projekat;
import rs.ac.bg.fon.ps.domain.User;
import rs.ac.bg.fon.ps.domain.Zadatak;
import rs.ac.bg.fon.ps.domain.enumeration.FrmMode;
import rs.ac.bg.fon.ps.view.form.FrmProjekat;
import rs.ac.bg.fon.ps.view.form.components.table.AktivnostTableModel;
import rs.ac.bg.fon.ps.view.form.components.table.ProjekatTableModel;

/**
 *
 * @author Anja
 */
public class FrmProjekatController {

    private final User currentUser;
    FrmProjekat frmProjekat;
    private ProjekatTableModel ptm;
    private Projekat proj;
    SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");

    public FrmProjekatController(User user) {
        currentUser = user;
    }

    public FrmProjekat getFrmProjekat() {
        return frmProjekat;
    }
    
    

    public Projekat getProj() {
        return proj;
    }

    public void setProj(Projekat projekat) {
        this.proj = projekat;
    }

    void openFrmProjekat(Projekat projekat, FrmMode frmMode, ProjekatTableModel ptm) throws Exception {
        frmProjekat = new FrmProjekat();
        frmProjekat.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.ptm = ptm;
        this.proj = projekat;
        addListenersFrmProjekat();
        prepareView(frmMode, projekat);
        frmProjekat.setVisible(true);

    }

    private void addListenersFrmProjekat() {
        frmProjekat.addBtnDodajNovuAktivnostActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    AktivnostTableModel atm = (AktivnostTableModel) frmProjekat.getTblAktivnosti().getModel();
                    Aktivnost a = new Aktivnost();
                    a.setProjekat(proj);
                    FormCoordinator.getInstance().openFrmAktivnost(a, FrmMode.FORM_ADD, atm);
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(frmProjekat, ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
                    return;
                }
            }
        });
        frmProjekat.addBtnIzmeniAktivnostActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int selectedRow = frmProjekat.getTblAktivnosti().getSelectedRow();
                if (selectedRow != -1) {
                    AktivnostTableModel atm = (AktivnostTableModel) frmProjekat.getTblAktivnosti().getModel();
                    Aktivnost aktivnost = atm.getAktivnost(selectedRow);

                    System.out.println(aktivnost.getProjekat());

                    try {
                        FormCoordinator.getInstance().openFrmAktivnost(aktivnost, FrmMode.FORM_UPDATE, atm);
                    } catch (Exception ex) {
                        JOptionPane.showMessageDialog(frmProjekat, ex.getMessage(), "Izmena aktivnosti " + aktivnost, JOptionPane.ERROR_MESSAGE);
                        return;
                    }
                } else {
                    JOptionPane.showMessageDialog(frmProjekat, "Morate označiti aktivnost za izmenu!", "Izmena aktivnosti", JOptionPane.ERROR_MESSAGE);
                }
            }
        });
        frmProjekat.addBtnObrisiAktivnostActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int selectedRow = frmProjekat.getTblAktivnosti().getSelectedRow();
                if (selectedRow != -1) {
                    AktivnostTableModel atm = (AktivnostTableModel) frmProjekat.getTblAktivnosti().getModel();
                    Aktivnost aktivnost = atm.getAktivnost(selectedRow);
                    System.out.println(aktivnost);
                    try {
                        int opt = JOptionPane.showConfirmDialog(frmProjekat, "Da li ste sigurni da želite da izvršite brisanje?", "Brisanje projekta", JOptionPane.YES_NO_OPTION);
                        if (opt == JOptionPane.YES_OPTION) {
                            Communication.getInstance().deleteAktivnost(aktivnost);
                            atm.deleteAktivnost(selectedRow);
                            List<Aktivnost> aktivnosti = atm.getAktivnosti();
                            atm.update(aktivnosti);
                        } else {
                            return;
                        }
                    } catch (Exception ex) {
                        ex.printStackTrace();
                        JOptionPane.showMessageDialog(frmProjekat, "Sistem ne moze da obrise aktivnost!", "Brisanje aktivnosti " + aktivnost, JOptionPane.ERROR_MESSAGE);
                        return;
                    }
                    JOptionPane.showMessageDialog(frmProjekat, "Uspešno obrisana aktivnost " + aktivnost + "!", "Brisanje aktivnosti", JOptionPane.INFORMATION_MESSAGE);
                } else {
                    JOptionPane.showMessageDialog(frmProjekat, "Morate označiti aktivnost za brisanje!", "Brisanje aktivnosti", JOptionPane.ERROR_MESSAGE);
                }
            }
        });
        frmProjekat.addBtnPrikaziDetaljeAktivnostiActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int selectedRow = frmProjekat.getTblAktivnosti().getSelectedRow();
                if (selectedRow != -1) {
                    AktivnostTableModel atm = (AktivnostTableModel) frmProjekat.getTblAktivnosti().getModel();
                    Aktivnost aktivnost = atm.getAktivnost(selectedRow);

                    System.out.println(aktivnost);
                    try {
                        FormCoordinator.getInstance().openFrmAktivnost(aktivnost, FrmMode.FORM_VIEW, atm);
                    } catch (Exception ex) {
                        ex.printStackTrace();
                        JOptionPane.showMessageDialog(frmProjekat, ex.getMessage(), "Pregled aktivnost " + aktivnost, JOptionPane.ERROR_MESSAGE);
                        return;
                    }
                } else {
                    JOptionPane.showMessageDialog(frmProjekat, "Morate označiti aktivnost za pregled!", "Pregled aktivnosti", JOptionPane.ERROR_MESSAGE);
                }

            }
        });
        frmProjekat.addBtnDodajProjekatActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    validate();
                    String sifra = frmProjekat.getTxtSifraProjekta().getText().trim();
                    String naziv = frmProjekat.getTxtNazivProjekta().getText().trim();
                    Date datum = null;

                    try {
                        datum = sdf.parse(frmProjekat.getTxtDatumOdrzavanja().getText());
                    } catch (ParseException ex) {
                        throw new Exception("Datum održavanja mora biti u formatu dd.MM.yyyy!");
                    }

                    ClanOrganizacije koor = (ClanOrganizacije) frmProjekat.getCbKoordinatorProjekta().getSelectedItem();

                    Projekat projekat = new Projekat(sifra, naziv, datum, koor);

                    AktivnostTableModel atm = (AktivnostTableModel) frmProjekat.getTblAktivnosti().getModel();
                    List<Aktivnost> aktivnostiProjekta = atm.getAktivnosti();
                    projekat.setAktivnosti(aktivnostiProjekta);
                    
                    for (int i =0; i<aktivnostiProjekta.size(); i++) {
                        for(int j=i+1; j<aktivnostiProjekta.size(); j++){
                            if(aktivnostiProjekta.get(i).equals(aktivnostiProjekta.get(j))){
                                throw new Exception("Postoje dve aktivnosti sa istom šifrom!");
                            }
                        }
                    }
                    
                    
                    for (Aktivnost aktivnost : aktivnostiProjekta) {
                        aktivnost.setProjekat(projekat);
                        for (Zadatak zadatak : aktivnost.getZadaci()) {
                            zadatak.setProjekat(projekat);
                            zadatak.setAktivnost(aktivnost);
                        }
                    }

                    try {

                        Communication.getInstance().addProjekat(projekat);

                        // ptm.addProjekat(projekat);
                        List<Projekat> projekti = getListProjekti();
                        ptm.update(projekti);
                        frmProjekat.dispose();
                        JOptionPane.showMessageDialog(frmProjekat, "Sistem je zapamtio projekat " + projekat + "!", "Dodavanje novog projekta", JOptionPane.INFORMATION_MESSAGE);
                        return;
                    } catch (Exception ex) {
                        ex.printStackTrace();
                        JOptionPane.showMessageDialog(frmProjekat, "Sistem ne može da zapamti projekat!"+ex.getMessage(), "Dodavanje novog projekta", JOptionPane.ERROR_MESSAGE);

                    }

                } catch (Exception ex) {
                    ex.printStackTrace();
                    JOptionPane.showMessageDialog(frmProjekat, "Sistem ne može da zapamti projekat! "+ex.getMessage(), "Sačuvaj projekat", JOptionPane.ERROR_MESSAGE);
                }
            }

        });
        frmProjekat.addBtnObrisiProjekatActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    Communication.getInstance().deleteProjekat(proj);
                    List<Projekat> projekti = getListProjekti();
                    ptm.update(projekti);
                    frmProjekat.dispose();
                    JOptionPane.showMessageDialog(frmProjekat, "Uspešno obrisan projekat " + proj + "!", "Brisanje projekta", JOptionPane.INFORMATION_MESSAGE);
                    return;
                } catch (Exception ex) {
                    ex.printStackTrace();
                    JOptionPane.showMessageDialog(frmProjekat, ex.getMessage(), "Brisanje projekta", JOptionPane.ERROR_MESSAGE);
                }
            }
        }
        );
        frmProjekat.addBtnSacuvajIzmeneActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    validate();
                    String sifra = frmProjekat.getTxtSifraProjekta().getText().trim();
                    String naziv = frmProjekat.getTxtNazivProjekta().getText().trim();
                    Date datum = null;

                    try {
                        datum = sdf.parse(frmProjekat.getTxtDatumOdrzavanja().getText());
                    } catch (ParseException ex) {
                        throw new Exception("Datum održavanja mora biti u formatu dd.MM.yyyy!");
                    }

                    ClanOrganizacije koor = (ClanOrganizacije) frmProjekat.getCbKoordinatorProjekta().getSelectedItem();

                    Projekat izmenjenProjekat = new Projekat(sifra, naziv, datum, koor);

                    AktivnostTableModel atm = (AktivnostTableModel) frmProjekat.getTblAktivnosti().getModel();
                    List<Aktivnost> aktivnostiProjekta = atm.getAktivnosti();
                    izmenjenProjekat.setAktivnosti(aktivnostiProjekta);
                    for (Aktivnost aktivnost : aktivnostiProjekta) {
                        aktivnost.setProjekat(izmenjenProjekat);
                        for (Zadatak zadatak : aktivnost.getZadaci()) {
                            zadatak.setProjekat(izmenjenProjekat);
                            zadatak.setAktivnost(aktivnost);
                        }
                    }

                    try {

                        Communication.getInstance().updateProjekat(proj, izmenjenProjekat);

                        // ptm.addProjekat(projekat);
                        List<Projekat> projekti = getListProjekti();
                        ptm.update(projekti);
                        frmProjekat.dispose();
                        JOptionPane.showMessageDialog(frmProjekat, "Sistem je zapamtio projekat!", "Izmena projekta", JOptionPane.INFORMATION_MESSAGE);
                        return;
                    } catch (Exception ex) {
                        ex.printStackTrace();
                        JOptionPane.showMessageDialog(frmProjekat, "Sistem ne može da zapamti projekat! ", "Izmena projekta", JOptionPane.ERROR_MESSAGE);

                    }

                } catch (Exception ex) {
                    ex.printStackTrace();
                    JOptionPane.showMessageDialog(frmProjekat, "Sistem ne može da zapamti projekat! ", "Izmeni projekat", JOptionPane.ERROR_MESSAGE);
                }
            }

        });
        frmProjekat.addBtnObrisiProjekatActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    Communication.getInstance().deleteProjekat(proj);
                    List<Projekat> projekti = getListProjekti();
                    ptm.update(projekti);
                    frmProjekat.dispose();
                    JOptionPane.showMessageDialog(frmProjekat, "Sistem je obrisao projekat " + proj + "!", "Brisanje projekta", JOptionPane.INFORMATION_MESSAGE);
                    return;
                } catch (Exception ex) {
                    ex.printStackTrace();
                    JOptionPane.showMessageDialog(frmProjekat, "Sistem ne može da obriše projekat!"+ex.getMessage(), "Brisanje projekta", JOptionPane.ERROR_MESSAGE);
                }

            }
        });
        frmProjekat.addBtnOmoguciIzmeneActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    frmProjekat.setTitle("Human Resources app - izmena projekta");
                    frmProjekat.getBtnOmoguciIzmene().setVisible(false);
                    frmProjekat.getBtnObrisiProjekat().setVisible(true);
                    frmProjekat.getBtnSacuvajIzmene().setVisible(true);
                    frmProjekat.getBtnDodajProjekat().setVisible(false);
                    frmProjekat.getCbKoordinatorProjekta().setEnabled(true);
                    frmProjekat.getTxtSifraProjekta().setEnabled(true);
                    frmProjekat.getTxtNazivProjekta().setEnabled(true);
                    frmProjekat.getTxtDatumOdrzavanja().setEnabled(true);

                    frmProjekat.getTblAktivnosti().setEnabled(true);
                    frmProjekat.getBtnDodajNovuAktivnost().setEnabled(true);
                    frmProjekat.getBtnIzmeniAktivnost().setEnabled(true);
                    frmProjekat.getBtnObrisiAktivnost().setEnabled(true);
                    frmProjekat.getBtnPrikaziDetaljeAktivnosti().setEnabled(true);
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(frmProjekat, ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
                    return;
                }
            }
        });
    }

    private void prepareView(FrmMode frmMode, Projekat projekat) throws Exception {
        frmProjekat.getLblCurrentUser().setText(currentUser.getFirstname() + " " + currentUser.getLastname());
        frmProjekat.getLblProjekat().setText(proj.getNazivProjekta());
        fillCbKoordinator();

        if (frmMode.equals(FrmMode.FORM_ADD)) {
            AktivnostTableModel atm = new AktivnostTableModel(new ArrayList<>());
            frmProjekat.getTblAktivnosti().setModel(atm);

            frmProjekat.setTitle("Human Resources app - dodaj novi projekat");
            frmProjekat.getBtnOmoguciIzmene().setVisible(false);
            frmProjekat.getBtnObrisiProjekat().setVisible(false);
            frmProjekat.getBtnSacuvajIzmene().setVisible(false);

        }
        if (frmMode.equals(FrmMode.FORM_UPDATE)) {
            AktivnostTableModel atm = new AktivnostTableModel(getListAktivnost(projekat));
            frmProjekat.getTblAktivnosti().setModel(atm);

            frmProjekat.setTitle("Human Resources app - izmena projekta");
            frmProjekat.getBtnOmoguciIzmene().setVisible(false);
            frmProjekat.getBtnObrisiProjekat().setVisible(true);
            frmProjekat.getBtnSacuvajIzmene().setVisible(true);
            frmProjekat.getBtnDodajProjekat().setVisible(false);
            
            frmProjekat.getTxtSifraProjekta().setText(projekat.getSifraProjekta());
            frmProjekat.getTxtDatumOdrzavanja().setText(sdf.format(projekat.getDatumOdrzavanja()));
            frmProjekat.getTxtNazivProjekta().setText(projekat.getNazivProjekta());
            frmProjekat.getCbKoordinatorProjekta().setSelectedItem(projekat.getKoordinatorProjekta());

        }
        if (frmMode.equals(FrmMode.FORM_VIEW)) {
            AktivnostTableModel atm = new AktivnostTableModel(getListAktivnost(projekat));
            frmProjekat.getTblAktivnosti().setModel(atm);

            frmProjekat.setTitle("Human Resources app - pregled projekta");
            frmProjekat.getBtnOmoguciIzmene().setVisible(true);
            frmProjekat.getBtnObrisiProjekat().setVisible(true);
            frmProjekat.getBtnSacuvajIzmene().setVisible(false);
            frmProjekat.getBtnDodajProjekat().setVisible(false);
            frmProjekat.getCbKoordinatorProjekta().setEnabled(false);
            frmProjekat.getTxtSifraProjekta().setEnabled(false);
            frmProjekat.getTxtNazivProjekta().setEnabled(false);
            frmProjekat.getTxtDatumOdrzavanja().setEnabled(false);

            frmProjekat.getTblAktivnosti().setEnabled(false);
            frmProjekat.getBtnDodajNovuAktivnost().setEnabled(false);
            frmProjekat.getBtnIzmeniAktivnost().setEnabled(false);
            frmProjekat.getBtnObrisiAktivnost().setEnabled(true);
            frmProjekat.getBtnPrikaziDetaljeAktivnosti().setEnabled(false);

            frmProjekat.getTxtSifraProjekta().setText(projekat.getSifraProjekta());
            frmProjekat.getTxtDatumOdrzavanja().setText(sdf.format(projekat.getDatumOdrzavanja()));
            frmProjekat.getTxtNazivProjekta().setText(projekat.getNazivProjekta());
            frmProjekat.getCbKoordinatorProjekta().setSelectedItem(projekat.getKoordinatorProjekta());

        }
    }

    private List<Aktivnost> getListAktivnost(Projekat projekat) throws Exception {
        List<Aktivnost> aktivnosti = new ArrayList<>();
        try {
            aktivnosti = Communication.getInstance().getAllAktivnostiProjekta(projekat);
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new Exception("\nDogodila se greska u komunikaciji!(server je ugasen)");
//            System.exit(0);
        }
        return aktivnosti;
    }

    private void fillCbKoordinator() throws Exception {
        frmProjekat.getCbKoordinatorProjekta().removeAllItems();
        List<ClanOrganizacije> clanovi = getListClanovi();
        for (ClanOrganizacije clanOrganizacije : clanovi) {
            frmProjekat.getCbKoordinatorProjekta().addItem(clanOrganizacije);
        }
    }

    private List<ClanOrganizacije> getListClanovi() throws Exception {
        List<ClanOrganizacije> clanovi = null;
        try {
            clanovi = Communication.getInstance().getAllClanovi();
        } catch (Exception ex) {
            throw new Exception("\nDogodila se greska u komunikaciji!(server je ugasen)");
//            System.exit(0);
        }
        return clanovi;
    }

    private void validate() throws Exception {
        StringBuilder sbErr = new StringBuilder();
        if (frmProjekat.getTxtSifraProjekta().getText().trim() == null) {
            sbErr.append("Morate uneti šifru projekta!\n");
        }
        if (frmProjekat.getTxtNazivProjekta().getText().trim() == null) {
            sbErr.append("Morate uneti naziv projekta!\n");
        }
        if (frmProjekat.getTxtDatumOdrzavanja().getText().trim() == null) {
            sbErr.append("Morate uneti datum održavanja!\n");
        }
        if (!sbErr.toString().isEmpty()) {
            throw new Exception(sbErr.toString());
        }
    }

    private List<Projekat> getListProjekti() throws Exception {
        List<Projekat> projekti = new ArrayList<>();
        try {
            projekti = Communication.getInstance().getAllProjekti();
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new Exception("\nDogodila se greska u komunikaciji!(server je ugasen)");
//            System.exit(0);
        }
        return projekti;
    }

}
