/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs.ac.bg.fon.ps.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import rs.ac.bg.fon.ps.domain.Aktivnost;
import rs.ac.bg.fon.ps.domain.ClanOrganizacije;
import rs.ac.bg.fon.ps.domain.IDomainObject;
import rs.ac.bg.fon.ps.domain.Projekat;
import rs.ac.bg.fon.ps.domain.Sektor;
import rs.ac.bg.fon.ps.domain.User;
import rs.ac.bg.fon.ps.domain.Zadatak;
import rs.ac.bg.fon.ps.operation.AbstractGenericOperation;
import rs.ac.bg.fon.ps.operation.clanOrganizacije.ZapamtiClanaOrganizacije;
import rs.ac.bg.fon.ps.operation.clanOrganizacije.ObrisiClanaOrganizacije;
import rs.ac.bg.fon.ps.operation.clanOrganizacije.UcitajListuClanovaOrganizacije;
import rs.ac.bg.fon.ps.operation.clanOrganizacije.IzmeniClanaOrganizacije;
import rs.ac.bg.fon.ps.operation.login.LoginUser;
import rs.ac.bg.fon.ps.operation.projekat.ZapamtiProjekat;
import rs.ac.bg.fon.ps.operation.projekat.ObrisiProjekat;
import rs.ac.bg.fon.ps.operation.projekat.UcitajListuProjekata;
import rs.ac.bg.fon.ps.operation.projekat.IzmeniProjekat;
import rs.ac.bg.fon.ps.operation.projekat.aktivnost.ObrisiAktivnost;
import rs.ac.bg.fon.ps.operation.projekat.aktivnost.UcitajListuAktivnostiProjekta;
import rs.ac.bg.fon.ps.operation.projekat.aktivnost.zadatak.UcitajListuZadatakaAktivnostiProjekta;
import rs.ac.bg.fon.ps.operation.sektor.UcitajListuSektoraOrganizacije;
import rs.ac.bg.fon.ps.operation.sektor.IzmeniSektor;

/**
 *
 * @author Anja
 */
public class Controller {
    private static Controller instance;

    private Controller() {
    }

    public static Controller getInstance() {
        if (instance == null) {
            instance = new Controller();
        }
        return instance;
    }

    public User login(User user) throws Exception {
        AbstractGenericOperation login = new LoginUser();
        login.execute(user);
        return ((LoginUser) login).getUser();
    }

    public List<IDomainObject> getClanovi(ClanOrganizacije clanOrganizacije) throws Exception {
        AbstractGenericOperation getClanovi = new UcitajListuClanovaOrganizacije();
        getClanovi.execute(clanOrganizacije);
        return ((UcitajListuClanovaOrganizacije) getClanovi).getClanovi();
    }

    public ClanOrganizacije deleteClan(ClanOrganizacije clanOrganizacije) throws Exception {
        AbstractGenericOperation deleteClan = new ObrisiClanaOrganizacije();
        deleteClan.execute(clanOrganizacije);
         return ((ObrisiClanaOrganizacije)deleteClan).getClan();
    }

    public List<IDomainObject> getSektori(Sektor sektor) throws Exception {
        AbstractGenericOperation getSektori = new UcitajListuSektoraOrganizacije();
        getSektori.execute(sektor);
        return ((UcitajListuSektoraOrganizacije) getSektori).getSektori();
    }

    public ClanOrganizacije addClan(ClanOrganizacije clanOrganizacije) throws Exception {
        AbstractGenericOperation addClan = new ZapamtiClanaOrganizacije();
        addClan.execute(clanOrganizacije);
        return ((ZapamtiClanaOrganizacije)addClan).getClan();
    }

    public ClanOrganizacije updateClan(ClanOrganizacije clanOrganizacije) throws Exception {
        AbstractGenericOperation updateClan = new IzmeniClanaOrganizacije();
        updateClan.execute(clanOrganizacije);
        return ((IzmeniClanaOrganizacije) updateClan).getClan();
    }

    public List<IDomainObject> getProjekti(Projekat projekat) throws Exception {
        AbstractGenericOperation getProjekti = new UcitajListuProjekata();
        getProjekti.execute(projekat);
        return ((UcitajListuProjekata) getProjekti).getProjekti();
    }

    public Projekat deleteProjekat(Projekat projekat) throws Exception {
        AbstractGenericOperation deleteProjekat = new ObrisiProjekat(projekat);
        deleteProjekat.execute(projekat);
        return ((ObrisiProjekat)deleteProjekat).getProjekat();
    }

    public List<IDomainObject> getAktivnostiProjekta(Aktivnost aktivnost, Projekat p) throws Exception {
        AbstractGenericOperation getAktivnosti = new UcitajListuAktivnostiProjekta(new ArrayList<>(), p);
        getAktivnosti.execute(aktivnost);
        return ((UcitajListuAktivnostiProjekta) getAktivnosti).getAktivnosti();
    }

    public Aktivnost deleteAktivnost(Aktivnost aktivnost) throws Exception {
        AbstractGenericOperation deleteAktivnost = new ObrisiAktivnost();
        deleteAktivnost.execute(aktivnost);
        return ((ObrisiAktivnost)deleteAktivnost).getAktivnost();
    }

    public List<IDomainObject> getZadaciAktivnostiProjekta(Zadatak zadatak, Aktivnost a) throws Exception {
        AbstractGenericOperation getZadaci = new UcitajListuZadatakaAktivnostiProjekta(new ArrayList<>(), a);
        getZadaci.execute(zadatak);
        return ((UcitajListuZadatakaAktivnostiProjekta) getZadaci).getZadaci();
    }

    public Projekat addProjekat(Projekat projekat) throws Exception {
        AbstractGenericOperation addProjekat = new ZapamtiProjekat(projekat);
        addProjekat.execute(projekat);
        return ((ZapamtiProjekat)addProjekat).getProjekat();
    }

    public Projekat updateProjekat(HashMap<String, Projekat> hashMap) throws Exception {
        Projekat proj = hashMap.get("novi");
        AbstractGenericOperation updateProjekat = new IzmeniProjekat(hashMap);
        updateProjekat.execute(proj);
        return ((IzmeniProjekat) updateProjekat).getNovi();
    }

    public Sektor updateSektor(HashMap<String, IDomainObject> hashMap) throws Exception {
        Sektor sektor = (Sektor) hashMap.get("sektor");
        AbstractGenericOperation updateSektor = new IzmeniSektor(hashMap);
        updateSektor.execute(sektor);
        return ((IzmeniSektor) updateSektor).getSektor();
    }
    
    
}
