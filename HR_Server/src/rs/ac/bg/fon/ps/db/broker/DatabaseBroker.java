/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs.ac.bg.fon.ps.db.broker;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import rs.ac.bg.fon.ps.db.connection.DbConnectionFactory;
import rs.ac.bg.fon.ps.domain.IDomainObject;

/**
 *
 * @author Anja
 */
public class DatabaseBroker {

    public IDomainObject getOne(IDomainObject domainObject) throws Exception {
        Connection connection = DbConnectionFactory.getInstance().getConnection();
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT *")
                .append(" FROM ")
                .append(domainObject.getTableName());

        if (domainObject.getNumberConnectedObject() > 0) {
            sb.append(domainObject.getJoinCondition());
            sb.append(domainObject.getWhereCondition());
            sb.append(domainObject.getKeyValue());
        } else {
            sb.append(" WHERE ")
                    .append(domainObject.getKeyName())
                    .append(" = ")
                    .append(domainObject.getKeyValue());
        }

        String query = sb.toString();
        System.out.println(query);

        Statement statement = connection.createStatement();
        ResultSet rs = statement.executeQuery(query);

        IDomainObject object = domainObject.makeForOne(rs);

        return object;
    }

    public List<IDomainObject> getAll(IDomainObject domainObject) throws Exception {
        Connection connection = DbConnectionFactory.getInstance().getConnection();
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT *")
                .append(" FROM ")
                .append(domainObject.getTableName());

        if (domainObject.getNumberConnectedObject() > 0) {
            sb.append(domainObject.getJoinCondition());
        }
        String query = sb.toString();
        System.out.println(query);
        Statement statement = connection.createStatement();
        ResultSet rs = statement.executeQuery(query);
        List<IDomainObject> objects = domainObject.makeList(rs);
        return objects;
    }

    public IDomainObject delete(IDomainObject domainObject) throws SQLException {
        Connection connection = DbConnectionFactory.getInstance().getConnection();
        StringBuilder sb = new StringBuilder();
        sb.append("DELETE FROM ")
                .append(domainObject.getTableName())
                .append(" WHERE ")
                .append(domainObject.getKeyName())
                .append(" = ")
                .append(domainObject.getKeyValue());    
        if (domainObject.getNumberOfKeys() == 2) {
            sb.append(" AND ").append(domainObject.getKeyName2())
                    .append(" = ")
                    .append(domainObject.getKeyValue2());
        }

        if (domainObject.getNumberOfKeys() > 2) {
            sb.append(" AND ").append(domainObject.getKeyName2())
                    .append(" = ")
                    .append(domainObject.getKeyValue2()).append(" AND ").append(domainObject.getKeyName3())
                    .append(" = ")
                    .append(domainObject.getKeyValue3());
        }
        String query = sb.toString();
        System.out.println(query);

        Statement statement = connection.createStatement();
        statement.executeUpdate(query);
        statement.close();
        return domainObject;
    }

    public IDomainObject add(IDomainObject domainObject) throws SQLException {
        try {
            Connection connection = DbConnectionFactory.getInstance().getConnection();
            StringBuilder sb = new StringBuilder();
            sb.append("INSERT INTO ")
                    .append(domainObject.getTableName())
                    .append(" (").append(domainObject.getAllColumnNames()).append(")")
                    .append(" VALUES (")
                    .append(domainObject.getValuesForInsert())
                    .append(")");
            String query = sb.toString();
            System.out.println(query);
            Statement statement = connection.createStatement();
            if (domainObject.autoincrement() == true) {
                statement.executeUpdate(query, Statement.RETURN_GENERATED_KEYS);
                ResultSet rs = statement.getGeneratedKeys();
                if (rs.next()) {
                    int id = rs.getInt(1);
                    domainObject.setId(id);
                }
                statement.close();
                return domainObject;
            } else {
                statement.executeUpdate(query);

                statement.close();
                return domainObject;
            }
        } catch (SQLException ex) {
            throw ex;
        } catch (Exception ex) {
            throw ex;
        }
    }

    public IDomainObject update(IDomainObject domainObject) throws SQLException {
        Connection connection = DbConnectionFactory.getInstance().getConnection();
        Statement statement;
        StringBuilder sb = new StringBuilder();
        sb.append("UPDATE ")
                .append(domainObject.getTableName())
                .append(" SET ")
                .append(domainObject.getColumnNameAndValuesForUpdate())
                .append(" WHERE ")
                .append(domainObject.getKeyName())
                .append(" = ")
                .append(domainObject.getKeyValue());
        String query = sb.toString();
        System.out.println(query);

        statement = connection.createStatement();
        statement.executeUpdate(query);

        return domainObject;
    }

    public IDomainObject update(IDomainObject domainObject, int number, int operation) throws SQLException {
        Connection connection = DbConnectionFactory.getInstance().getConnection();
        Statement statement;
        StringBuilder sb = new StringBuilder();
        sb.append("UPDATE ")
                .append(domainObject.getTableName())
                .append(" SET ");
        if (operation == 1) {
            sb.append(domainObject.getKeyValue3()+"+"+ number);  
        }
        if (operation == 2) {
            number = (-1)*number;
            sb.append(domainObject.getKeyValue3()+"+"+ number);
        }
        if(operation == 0){
            sb.append(domainObject.getKeyValue3()+"="+ number);
        }
        sb.append(" WHERE ")
                .append(domainObject.getKeyName())
                .append(" = ")
                .append(domainObject.getKeyValue());
        String query = sb.toString();
        System.out.println(query);

        statement = connection.createStatement();
        statement.executeUpdate(query);

        return domainObject;
    }

    public List<IDomainObject> getAll(IDomainObject domainObject, Object o) throws SQLException, Exception {
        Connection connection = DbConnectionFactory.getInstance().getConnection();
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT *") //select * from zadatak where sifraProjekta=o.getproj.getsifra AND sifraAktivnosti=o.getsifra
                .append(" FROM ")
                .append(domainObject.getTableName());

        if (domainObject.getNumberConnectedObject() > 0) {
            sb.append(domainObject.getJoinCondition());
        }
        if (domainObject.getNumberConnectedObject() == 2) {
            sb.append(" WHERE ")
                    .append(domainObject.getKeyName2())
                    .append(" = ")
                    .append(domainObject.getKeyValue2(o));
        }
        if (domainObject.getNumberConnectedObject() > 2) {
            sb.append(" WHERE ")
                    .append(domainObject.getKeyName2())
                    .append(" = ")
                    .append(domainObject.getKeyValue2(o)).append(" AND ")
                    .append(domainObject.getKeyName())
                    .append(" = ")
                    .append(domainObject.getKeyValue(o));
        }

        String query = sb.toString();

        System.out.println(query);

        Statement statement = connection.createStatement();
        ResultSet rs = statement.executeQuery(query);

        List<IDomainObject> objects = domainObject.makeList(rs, o);

        return objects;
    }

}
