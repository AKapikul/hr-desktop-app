/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs.ac.bg.fon.ps.db.connection;

import java.io.FileInputStream;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Anja
 */
public class Settings {
    private static Settings instance;
    private Properties props;

    private Settings() {
        try {
            props = new Properties();
            props.load(new FileInputStream("config/dbconfig.properties"));
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public static Settings getInstance() {
        if (instance == null) {
            instance = new Settings();
        }
        return instance;
    }

    public String getProperty(String key) {
        return props.getProperty(key, "n/a");
    }
    
    public void setProperty(String key, String val) {
        props.setProperty(key, val);
    }
}
