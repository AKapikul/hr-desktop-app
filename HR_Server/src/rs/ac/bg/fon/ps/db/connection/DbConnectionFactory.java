/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs.ac.bg.fon.ps.db.connection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author Anja
 */
public class DbConnectionFactory {
    private Connection connection;
    public static DbConnectionFactory instance;

    private DbConnectionFactory() throws SQLException {
        try {
            String url = Settings.getInstance().getProperty("url");
            String username = Settings.getInstance().getProperty("username");
            String password = Settings.getInstance().getProperty("password");
            connection = DriverManager.getConnection(url, username, password);
        } catch (SQLException ex) {
            throw new SQLException("Connection is not created!");
        }
    }

    public static DbConnectionFactory getInstance() throws SQLException {
        if (instance == null) {
            instance = new DbConnectionFactory();
        }
        return instance;
    }
    
        public Connection getConnection() {
        return connection;
    }
}
