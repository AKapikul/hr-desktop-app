/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs.ac.bg.fon.ps.server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import rs.ac.bg.fon.ps.db.connection.Settings;
import rs.ac.bg.fon.ps.threads.ProcessRequests;

/**
 *
 * @author Anja
 */
public class Server extends Thread{

    private List<ProcessRequests> clients;
    private boolean active = true;
    private ServerSocket serverSocket;

    public Server() {
        this.clients = new ArrayList<>();
    }

    @Override
    public void run() {
        try {
            serverSocket = new ServerSocket(Integer.parseInt(Settings.getInstance().getProperty("port")));
            while (active) {
                System.out.println("Waiting for connection...");
                Socket socket = serverSocket.accept();
                System.out.println("Connected!");
                handleClient(socket);

            }
        } catch (Exception ex) {
            ex.printStackTrace();
            System.out.println("Server is crashed!");
        }
     
        
    }

    private void handleClient(Socket socket) throws Exception {
        ProcessRequests processRequests = new ProcessRequests(socket);
        processRequests.start();
        clients.add(processRequests);
    }

    public void stopServer() {
        try {
            active = false;
            serverSocket.close();
            clients.forEach(Thread::interrupt);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void sendShutDownMessage() {
        for (ProcessRequests client : clients) {
            client.theEnd();
        }
    }

    
}
