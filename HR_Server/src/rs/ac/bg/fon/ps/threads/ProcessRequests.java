/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs.ac.bg.fon.ps.threads;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.HashMap;
import java.util.List;
import rs.ac.bg.fon.ps.communication.Operation;
import rs.ac.bg.fon.ps.communication.Request;
import rs.ac.bg.fon.ps.communication.Response;
import rs.ac.bg.fon.ps.controller.Controller;
import rs.ac.bg.fon.ps.domain.Aktivnost;
import rs.ac.bg.fon.ps.domain.ClanOrganizacije;
import rs.ac.bg.fon.ps.domain.IDomainObject;
import rs.ac.bg.fon.ps.domain.Projekat;
import rs.ac.bg.fon.ps.domain.Sektor;
import rs.ac.bg.fon.ps.domain.User;
import rs.ac.bg.fon.ps.domain.Zadatak;

/**
 *
 * @author Anja
 */
public class ProcessRequests extends Thread {

    private Socket socket;
    private final ObjectInputStream objectInputStream;
    private ObjectOutputStream objectOutputStream;
    private User loginUser;
    
    public Socket getSoket() {
        return socket;
    }

    public void setSoket(Socket soket) {
        this.socket = soket;
    }

    public ProcessRequests(Socket socket) throws IOException {
        this.socket = socket;
        objectInputStream = new ObjectInputStream(socket.getInputStream());
        objectOutputStream = new ObjectOutputStream(socket.getOutputStream());
    }

    @Override
    public void run() {
        while (!socket.isClosed()) {
            try {
                while (!isInterrupted()) {
                    Request request = (Request) objectInputStream.readObject();
                    Response response = handle(request);
                    objectOutputStream.writeObject(response);
                }
            } catch (Exception ex) {
                ex.printStackTrace();

            }
        }
    }

    private Response handle(Request request) {
        Operation operation = request.getOperation();
        switch (operation) {
            case LOGIN:
                return login((User) request.getArgument());
            case GET_ALL_CLANOVI:
                return getClanovi((List<ClanOrganizacije>) request.getArgument());
            case DELETE_CLAN:
                return deleteClan((ClanOrganizacije) request.getArgument());
            case GET_ALL_SEKTORI:
                return getSektori((List<Sektor>) request.getArgument());
            case ADD_CLAN:
                return addClan((ClanOrganizacije)request.getArgument());
            case UPDATE_CLAN:
                return updateClan((ClanOrganizacije)request.getArgument());
            case GET_ALL_PROJEKTI:
                return getProjekti((List<Projekat>) request.getArgument());
            case DELETE_PROJEKAT:
                return deleteProjekat((Projekat) request.getArgument());
            case GET_ALL_AKTIVNOSTI_PROJEKTA:
                return getAllAktivnostiProjekta(request.getArgument());
            case DELETE_AKTIVNOST:
                return deleteAktivnost((Aktivnost)request.getArgument());
            case GET_ALL_ZADACI_AKTIVNOSTI_PROJEKTA:
                return getAllZadaciAktivnostiProjekta(request.getArgument());
            case ADD_PROJEKAT:
                return addProjekat((Projekat)request.getArgument());
            case UPDATE_PROJEKAT:
                return updateProjekat((HashMap<String, Projekat>)request.getArgument());
            case UPDATE_SEKTOR:
                return updateSektor((HashMap<String, IDomainObject>)request.getArgument());
        }
        return null;
    }

    public Socket getSocket() {
        return socket;
    }

    public User getLoginUser() {
        return loginUser;
    }

    public void theEnd() {
        ObjectOutputStream out = null;
        try {
            out = new ObjectOutputStream(socket.getOutputStream());
            Response res = new Response();
            res.setResult("Gasenje!");
            out.writeObject(res);
        } catch (IOException ex) {
            System.out.println("Greska u komunikaciji");
        } finally {
            try {
                out.close();
            } catch (IOException ex) {
                System.out.println("Greska u komunikaciji");
            }
        }
    }
    

    private void sendResponse(Response response) throws IOException {
        ObjectOutputStream outSocket = new ObjectOutputStream(socket.getOutputStream());
        outSocket.writeObject(response);
    }

    private Response login(User user) {
        Response response = new Response();
        try {
            User user1 = Controller.getInstance().login(user);
            response.setResult(user1);
            //response.setStatus(ResponseStatus.SUCCESS);
            loginUser = user;
        } catch (Exception ex) {
            //  response.setStatus(ResponseStatus.ERROR);
            response.setException(ex);
        }

        return response;
    }

    private Response getClanovi(List<ClanOrganizacije> list) {
        Response response = new Response();
        try {
            List<IDomainObject> clanovi = Controller.getInstance().getClanovi(new ClanOrganizacije());

            response.setResult(clanovi);
        } catch (Exception ex) {
            response.setException(ex);
        }
        return response;
    }

    private Response deleteClan(ClanOrganizacije clanOrganizacije) {
        Response response = new Response();
        try {
            ClanOrganizacije c = Controller.getInstance().deleteClan(clanOrganizacije);
             response.setResult(c);
        } catch (Exception ex) {
            ex.printStackTrace();
            response.setException(ex);
        }
        return response;
    }

    private Response getSektori(List<Sektor> list) {
        Response response = new Response();
        try {
            List<IDomainObject> sektori = Controller.getInstance().getSektori(new Sektor());
            if (!sektori.isEmpty()) {
                response.setResult(sektori);

            } else {
                System.out.println("Nema sektora");
            }
        } catch (Exception ex) {
            response.setException(ex);
        }
        return response;
    }

    private Response addClan(ClanOrganizacije clanOrganizacije) {
        Response response = new Response();
        try{
            ClanOrganizacije clan = Controller.getInstance().addClan(clanOrganizacije);
            response.setResult(clan);
        } catch(Exception ex){
            ex.printStackTrace();
            response.setException(ex);
        }
        return response;
    }

    private Response updateClan(ClanOrganizacije clanOrganizacije) {
        Response response = new Response();
        try {
            ClanOrganizacije cl = Controller.getInstance().updateClan(clanOrganizacije);
            response.setResult(cl);
        } catch (Exception ex) {
            response.setException(ex);
        }
        return response;
    }

    private Response getProjekti(List<Projekat> list) {
        Response response = new Response();
        try {
            List<IDomainObject> projekti = Controller.getInstance().getProjekti(new Projekat());

            response.setResult(projekti);
        } catch (Exception ex) {
            response.setException(ex);
        }
        return response;
    }

    private Response deleteProjekat(Projekat projekat) {
        Response response = new Response();
        try {
            Projekat p = Controller.getInstance().deleteProjekat(projekat);
             response.setResult(p);
        } catch (Exception ex) {
            ex.printStackTrace();
            response.setException(ex);
        }
        return response;
    }

    private Response getAllAktivnostiProjekta(Object argument) {
        Projekat p = (Projekat) argument;
        Response response = new Response();
        try {
            List<IDomainObject> aktivnosti = Controller.getInstance().getAktivnostiProjekta(new Aktivnost(),p);

            response.setResult(aktivnosti);
        } catch (Exception ex) {
            response.setException(ex);
        }
        return response;
    }

    private Response deleteAktivnost(Aktivnost aktivnost) {
        Response response = new Response();
        try {
            Aktivnost a = Controller.getInstance().deleteAktivnost(aktivnost);
            response.setResult(a);
        } catch (Exception ex) {
            ex.printStackTrace();
            response.setException(ex);
        }
        return response;
    }

    private Response getAllZadaciAktivnostiProjekta(Object argument) {
        Aktivnost a = (Aktivnost) argument;
        Response response = new Response();
        try {
            List<IDomainObject> zadaci = Controller.getInstance().getZadaciAktivnostiProjekta(new Zadatak(),a);

            response.setResult(zadaci);
        } catch (Exception ex) {
            response.setException(ex);
        }
        return response;
    }

    private Response addProjekat(Projekat projekat) {
        Response response = new Response();
        try{
            Projekat proj = Controller.getInstance().addProjekat(projekat);
            response.setResult(proj);
        } catch(Exception ex){
            ex.printStackTrace();
            response.setException(ex);
        }
        return response;
    }

    private Response updateProjekat(HashMap<String, Projekat> hashMap) {
        Response response = new Response();
        try {
            Projekat pr = Controller.getInstance().updateProjekat(hashMap);
            response.setResult(pr);
        } catch (Exception ex) {
            response.setException(ex);
        }
        return response;
    }

    private Response updateSektor(HashMap<String, IDomainObject> hashMap) {
        Response response = new Response();
        try {
            Sektor s = Controller.getInstance().updateSektor(hashMap);
            response.setResult(s);
        } catch (Exception ex) {
            response.setException(ex);
        }
        return response;
    }



    
}
