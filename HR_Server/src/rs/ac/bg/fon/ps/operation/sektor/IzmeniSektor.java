/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs.ac.bg.fon.ps.operation.sektor;

import java.util.HashMap;
import rs.ac.bg.fon.ps.domain.ClanOrganizacije;
import rs.ac.bg.fon.ps.domain.IDomainObject;
import rs.ac.bg.fon.ps.domain.Sektor;
import rs.ac.bg.fon.ps.operation.AbstractGenericOperation;

/**
 *
 * @author Anja
 */
public class IzmeniSektor extends AbstractGenericOperation{
    Sektor sektor;
    ClanOrganizacije cl;
    int id;

    public IzmeniSektor(HashMap<String, IDomainObject> hashMap) {
        sektor = (Sektor) hashMap.get("sektor");
        cl = (ClanOrganizacije) hashMap.get("clan");
        id = cl.getClanID();
    }

    public Sektor getSektor() {
        return sektor;
    }
    
    
    @Override
    protected void preconditions(IDomainObject dobject) throws Exception {
        
    }

    @Override
    protected void executeOperation(IDomainObject dobject) throws Exception {
        sektor = (Sektor) databaseBroker.update((Sektor)dobject, id, 0);
    }
    
    
}
