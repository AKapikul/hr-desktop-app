/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs.ac.bg.fon.ps.operation.projekat.aktivnost;

import rs.ac.bg.fon.ps.domain.Aktivnost;
import rs.ac.bg.fon.ps.domain.IDomainObject;
import rs.ac.bg.fon.ps.operation.AbstractGenericOperation;

/**
 *
 * @author Anja
 */
public class ObrisiAktivnost extends AbstractGenericOperation{
    private Aktivnost aktivnost;

    public Aktivnost getAktivnost() {
        return aktivnost;
    }
    
    
    @Override
    protected void preconditions(IDomainObject dobject) throws Exception {
        
    }

    @Override
    protected void executeOperation(IDomainObject dobject) throws Exception {
        aktivnost = (Aktivnost) databaseBroker.delete((Aktivnost)dobject);
    }
    
}
