/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs.ac.bg.fon.ps.operation.projekat;

import java.util.List;
import rs.ac.bg.fon.ps.domain.IDomainObject;
import rs.ac.bg.fon.ps.domain.Projekat;
import rs.ac.bg.fon.ps.operation.AbstractGenericOperation;

/**
 *
 * @author Anja
 */
public class UcitajListuProjekata extends AbstractGenericOperation{
    List<IDomainObject> projekti;
    
    
    @Override
    protected void preconditions(IDomainObject dobject) throws Exception {
    }

    @Override
    protected void executeOperation(IDomainObject dobject) throws Exception {
        projekti = databaseBroker.getAll((Projekat)dobject);
    }

    public List<IDomainObject> getProjekti() {
        return projekti;
    }
    
    
}
