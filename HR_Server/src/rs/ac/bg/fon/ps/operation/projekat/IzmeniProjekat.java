/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs.ac.bg.fon.ps.operation.projekat;

import java.util.HashMap;
import rs.ac.bg.fon.ps.domain.IDomainObject;
import rs.ac.bg.fon.ps.domain.Projekat;
import rs.ac.bg.fon.ps.operation.AbstractGenericOperation;

/**
 *
 * @author Anja
 */
public class IzmeniProjekat extends AbstractGenericOperation{
    private Projekat stari;
    private Projekat novi;
    
    public IzmeniProjekat(HashMap<String, Projekat> hashMap) {
        stari = hashMap.get("stari");
        novi = hashMap.get("novi");
    }

    public Projekat getNovi() {
        return novi;
    }
    
    

    @Override
    protected void preconditions(IDomainObject dobject) throws Exception {
    }

    @Override
    protected void executeOperation(IDomainObject dobject) throws Exception {
        databaseBroker.delete(stari);
        novi = (Projekat) databaseBroker.add((Projekat)dobject);
    }
    
}
