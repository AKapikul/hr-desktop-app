/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs.ac.bg.fon.ps.operation.projekat;

import java.sql.Array;
import java.util.ArrayList;
import java.util.List;
import rs.ac.bg.fon.ps.domain.Aktivnost;
import rs.ac.bg.fon.ps.domain.ClanOrganizacije;
import rs.ac.bg.fon.ps.domain.IDomainObject;
import rs.ac.bg.fon.ps.domain.Projekat;
import rs.ac.bg.fon.ps.domain.Zadatak;
import rs.ac.bg.fon.ps.operation.AbstractGenericOperation;

/**
 *
 * @author Anja
 */
public class ZapamtiProjekat extends AbstractGenericOperation {

    private Projekat projekat;
    private List<Aktivnost> listaAktivnosti;
    private List<Zadatak> listaZadataka;

    public ZapamtiProjekat(Projekat projekat) {
        this.projekat = projekat;
        this.listaAktivnosti = projekat.getAktivnosti();
        this.listaZadataka = new ArrayList<>();
    }
    
    @Override
    protected void preconditions(IDomainObject dobject) throws Exception {

    }

    @Override
    protected void executeOperation(IDomainObject dobject) throws Exception {
        projekat = (Projekat) databaseBroker.add((Projekat) dobject);
        ClanOrganizacije clan = new ClanOrganizacije();
        for (Aktivnost aktivnost : listaAktivnosti) {
            aktivnost = (Aktivnost) databaseBroker.add(aktivnost);
            listaZadataka = aktivnost.getZadaci();
            for (Zadatak zadatak : listaZadataka) {
                zadatak = (Zadatak) databaseBroker.add(zadatak);
                clan = (ClanOrganizacije) databaseBroker.update(zadatak.getIzvrsitelj(), zadatak.getBrojBodova(), 1);
            }
        }
    }

    public Projekat getProjekat() {
        return projekat;
    }
}
