/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs.ac.bg.fon.ps.operation.projekat.aktivnost.zadatak;

import java.util.List;
import rs.ac.bg.fon.ps.domain.Aktivnost;
import rs.ac.bg.fon.ps.domain.IDomainObject;
import rs.ac.bg.fon.ps.domain.Zadatak;
import rs.ac.bg.fon.ps.operation.AbstractGenericOperation;

/**
 *
 * @author Anja
 */
public class UcitajListuZadatakaAktivnostiProjekta extends AbstractGenericOperation{
    List<IDomainObject> zadaci;
    Aktivnost a;

    public UcitajListuZadatakaAktivnostiProjekta(List<IDomainObject> zadaci, Aktivnost a) {
        this.zadaci = zadaci;
        this.a = a;
    }

    
    
    @Override
    protected void preconditions(IDomainObject dobject) throws Exception {
        
    }

    @Override
    protected void executeOperation(IDomainObject dobject) throws Exception {
        zadaci = databaseBroker.getAll((Zadatak)dobject, a);
    }

    public List<IDomainObject> getZadaci() {
        return zadaci;
    }
    
    
    
}
