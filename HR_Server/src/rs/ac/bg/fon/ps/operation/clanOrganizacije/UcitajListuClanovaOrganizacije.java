/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs.ac.bg.fon.ps.operation.clanOrganizacije;

import java.util.List;
import rs.ac.bg.fon.ps.domain.ClanOrganizacije;
import rs.ac.bg.fon.ps.domain.IDomainObject;
import rs.ac.bg.fon.ps.operation.AbstractGenericOperation;

/**
 *
 * @author Anja
 */
public class UcitajListuClanovaOrganizacije extends AbstractGenericOperation{
    List<IDomainObject> clanovi;

    @Override
    protected void preconditions(IDomainObject dobject) throws Exception {
        
    }

    @Override
    protected void executeOperation(IDomainObject dobject) throws Exception {
        clanovi = databaseBroker.getAll((ClanOrganizacije) dobject);
    }

    public List<IDomainObject> getClanovi() {
        return clanovi;
    }
    
}
