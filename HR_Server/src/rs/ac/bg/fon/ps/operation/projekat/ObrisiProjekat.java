/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs.ac.bg.fon.ps.operation.projekat;

import java.util.List;
import rs.ac.bg.fon.ps.domain.Aktivnost;
import rs.ac.bg.fon.ps.domain.ClanOrganizacije;
import rs.ac.bg.fon.ps.domain.IDomainObject;
import rs.ac.bg.fon.ps.domain.Projekat;
import rs.ac.bg.fon.ps.domain.Zadatak;
import rs.ac.bg.fon.ps.operation.AbstractGenericOperation;

/**
 *
 * @author Anja
 */
public class ObrisiProjekat extends AbstractGenericOperation {

    private Projekat projekat;
    private List<Aktivnost> listaAktivnosti;
    private List<Zadatak> listaZadataka;

    public Projekat getProjekat() {
        return projekat;
    }

    
    
    public ObrisiProjekat(Projekat projekat) {
        this.projekat = projekat;
        this.listaAktivnosti = projekat.getAktivnosti();
    }

    @Override
    protected void preconditions(IDomainObject dobject) throws Exception {
    }

    @Override
    protected void executeOperation(IDomainObject dobject) throws Exception {
        projekat = (Projekat) databaseBroker.delete((Projekat) dobject);
        ClanOrganizacije clan = new ClanOrganizacije();

        for (Aktivnost aktivnost : listaAktivnosti) {
            listaZadataka = aktivnost.getZadaci();
            for (Zadatak zadatak : listaZadataka) {
                clan = (ClanOrganizacije) databaseBroker.update(zadatak.getIzvrsitelj(), zadatak.getBrojBodova(), 2);

            }
        }
    }

}
