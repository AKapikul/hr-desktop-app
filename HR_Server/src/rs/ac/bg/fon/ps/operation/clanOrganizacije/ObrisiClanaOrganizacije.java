/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs.ac.bg.fon.ps.operation.clanOrganizacije;

import rs.ac.bg.fon.ps.domain.ClanOrganizacije;
import rs.ac.bg.fon.ps.domain.IDomainObject;
import rs.ac.bg.fon.ps.operation.AbstractGenericOperation;

/**
 *
 * @author Anja
 */
public class ObrisiClanaOrganizacije extends AbstractGenericOperation {
   private ClanOrganizacije clan;

    public ClanOrganizacije getClan() {
        return clan;
    }
   

    @Override
    protected void preconditions(IDomainObject dobject) throws Exception {
        
    }

    @Override
    protected void executeOperation(IDomainObject dobject) throws Exception {
        clan = (ClanOrganizacije) databaseBroker.delete((ClanOrganizacije)dobject);
    }
    
    
    
}
