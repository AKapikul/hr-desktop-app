/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs.ac.bg.fon.ps.operation.sektor;

import java.util.List;
import rs.ac.bg.fon.ps.domain.IDomainObject;
import rs.ac.bg.fon.ps.domain.Sektor;
import rs.ac.bg.fon.ps.operation.AbstractGenericOperation;

/**
 *
 * @author Anja
 */
public class UcitajListuSektoraOrganizacije extends AbstractGenericOperation{
    List<IDomainObject> sektori;
    
    @Override
    protected void preconditions(IDomainObject dobject) throws Exception {
        
    }

    @Override
    protected void executeOperation(IDomainObject dobject) throws Exception {
        sektori = databaseBroker.getAll((Sektor)dobject);
    }

    public List<IDomainObject> getSektori() {
        return sektori;
    }
    
}
