/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs.ac.bg.fon.ps.operation;

import java.sql.SQLException;
import rs.ac.bg.fon.ps.db.broker.DatabaseBroker;
import rs.ac.bg.fon.ps.db.connection.DbConnectionFactory;
import rs.ac.bg.fon.ps.domain.IDomainObject;

/**
 *
 * @author Anja
 */
public abstract class AbstractGenericOperation {
    protected DatabaseBroker databaseBroker;

    public AbstractGenericOperation() {
        databaseBroker = new DatabaseBroker();
    }

    public final void execute(IDomainObject dobject) throws Exception{
        try {
            preconditions(dobject);
            startTransaction();
            executeOperation(dobject);
            commitTransaction();
        } catch (Exception ex) {
            rollbackTransaction();
            throw ex;
        }
    }

    protected abstract void preconditions(IDomainObject dobject) throws Exception;

    private void startTransaction() throws SQLException {
        DbConnectionFactory.getInstance().getConnection().setAutoCommit(false);
    }

    protected abstract void executeOperation(IDomainObject dobject)  throws Exception;

    private void commitTransaction() throws SQLException {
        DbConnectionFactory.getInstance().getConnection().commit();
    }

    private void rollbackTransaction() throws SQLException {
        DbConnectionFactory.getInstance().getConnection().rollback();
    }
}
