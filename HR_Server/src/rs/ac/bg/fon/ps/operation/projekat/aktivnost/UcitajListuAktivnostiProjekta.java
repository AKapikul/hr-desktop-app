/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs.ac.bg.fon.ps.operation.projekat.aktivnost;

import java.util.List;
import rs.ac.bg.fon.ps.domain.Aktivnost;
import rs.ac.bg.fon.ps.domain.IDomainObject;
import rs.ac.bg.fon.ps.domain.Projekat;
import rs.ac.bg.fon.ps.operation.AbstractGenericOperation;

/**
 *
 * @author Anja
 */
public class UcitajListuAktivnostiProjekta extends AbstractGenericOperation{
    List<IDomainObject> aktivnosti;
    Projekat p;

    public UcitajListuAktivnostiProjekta(List<IDomainObject> aktivnosti, Projekat p) {
        this.aktivnosti = aktivnosti;
        this.p = p;
    }
    

    @Override
    protected void preconditions(IDomainObject dobject) throws Exception {
    }

    @Override
    protected void executeOperation(IDomainObject dobject) throws Exception {
         aktivnosti = databaseBroker.getAll((Aktivnost)dobject, p);
    }

    public List<IDomainObject> getAktivnosti() {
        return aktivnosti;
    }
    
    
}
