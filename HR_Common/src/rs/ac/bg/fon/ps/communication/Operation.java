/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs.ac.bg.fon.ps.communication;

import java.io.Serializable;

/**
 *
 * @author Anja
 */
public enum Operation implements Serializable{
    LOGIN,
    GET_ALL_CLANOVI,
    DELETE_CLAN,
    GET_ALL_SEKTORI,
    ADD_CLAN,
    UPDATE_CLAN,
    GET_ALL_PROJEKTI,
    DELETE_PROJEKAT,
    GET_ALL_AKTIVNOSTI_PROJEKTA,
    DELETE_AKTIVNOST,
    GET_ALL_ZADACI_AKTIVNOSTI_PROJEKTA,
    ADD_PROJEKAT,
    UPDATE_PROJEKAT,
    UPDATE_SEKTOR
    
    
}
