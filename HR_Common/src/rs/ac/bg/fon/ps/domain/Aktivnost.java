/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs.ac.bg.fon.ps.domain;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 *
 * @author Anja
 */
public class Aktivnost implements IDomainObject{
    private Projekat projekat;
    private String sifraAktivnosti;
    private String nazivAktivnosti;
    private String opisAktivnosti;
    private List<Zadatak> zadaci;

    public Aktivnost() {
        this.zadaci = new ArrayList<>();
    }

    public Aktivnost(Projekat projekat, String sifraAktivnosti, String nazivAktivnosti, String opisAktivnosti) {
        this.projekat = projekat;
        this.sifraAktivnosti = sifraAktivnosti;
        this.nazivAktivnosti = nazivAktivnosti;
        this.opisAktivnosti = opisAktivnosti;
        this.zadaci = new ArrayList<>();
    }

    public Projekat getProjekat() {
        return projekat;
    }

    public void setProjekat(Projekat projekat) {
        this.projekat = projekat;
    }

    public String getSifraAktivnosti() {
        return sifraAktivnosti;
    }

    public void setSifraAktivnosti(String sifraAktivnosti) {
        this.sifraAktivnosti = sifraAktivnosti;
    }

    public String getNazivAktivnosti() {
        return nazivAktivnosti;
    }

    public void setNazivAktivnosti(String nazivAktivnosti) {
        this.nazivAktivnosti = nazivAktivnosti;
    }

    public String getOpisAktivnosti() {
        return opisAktivnosti;
    }

    public void setOpisAktivnosti(String opisAktivnosti) {
        this.opisAktivnosti = opisAktivnosti;
    }

    public List<Zadatak> getZadaci() {
        return zadaci;
    }

    public void setZadaci(List<Zadatak> zadaci) {
        this.zadaci = zadaci;
    }
    
    

    @Override
    public int hashCode() {
        int hash = 5;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Aktivnost other = (Aktivnost) obj;
        if (!Objects.equals(this.sifraAktivnosti, other.sifraAktivnosti)) {
            return false;
        }
        if (!Objects.equals(this.projekat, other.projekat)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return nazivAktivnosti;
    }

    @Override
    public String getTableName() {
        return "aktivnost";
    }

    @Override
    public int getNumberConnectedObject() {
        return 2;
    }

    @Override
    public String getJoinCondition() {
        return "";
    }

    @Override
    public String getWhereCondition() {
        return "";
    }

    @Override
    public String getKeyValue() {
        return "'"+getSifraAktivnosti()+"'";
    }

    @Override
    public String getKeyName() {
        return "sifraAktivnosti";
    }

    @Override
    public IDomainObject makeForOne(ResultSet rs) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<IDomainObject> makeList(ResultSet rs) throws SQLException {
        List<IDomainObject> aktivnosti = new ArrayList<>();
        while (rs.next()) {
        
            String sifraA = rs.getString("sifraAktivnosti");
            String nazivA = rs.getString("nazivAktivnosti");
            String opisA = rs.getString("opisAktivnosti");

            Projekat p = new Projekat();
            p.setSifraProjekta(rs.getString("sifraProjekta"));
            p.setNazivProjekta(rs.getString("nazivProjekta"));
            p.setDatumOdrzavanja(rs.getDate("datumOdrzavanja"));
            
            ClanOrganizacije koord = new ClanOrganizacije();
            koord.setClanID(rs.getInt("clanID"));
            koord.setImePrezime(rs.getString("imePrezime"));
            koord.setDatumUclanjenja(rs.getDate("datumUclanjenja"));
            koord.setDatumIsclanjenja(rs.getDate("datumIsclanjenja"));
            koord.setKontakt(rs.getString("kontakt"));
            
            Sektor s = new Sektor();
            s.setSifraSektora(rs.getString("sifraSektora"));
            s.setPunNaziv(rs.getString("punNaziv"));
            s.setKoordinatorSektoraID(rs.getInt("koordinatorSektoraID"));

            koord.setSektor(s);
            p.setKoordinatorProjekta(koord);

            Aktivnost akt = new Aktivnost();
            akt.setSifraAktivnosti(sifraA);
            akt.setProjekat(p);
            akt.setNazivAktivnosti(nazivA);
            akt.setOpisAktivnosti(opisA);

            aktivnosti.add(akt);
        }
        return aktivnosti;
    }

    @Override
    public String getKeyName2() {
        return "sifraProjekta";
    }

    @Override
    public String getKeyValue2() {
        return "'"+getProjekat().getSifraProjekta()+"'";
    }

    @Override
    public String getKeyName3() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getKeyValue3() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getAllColumnNames() {
        return "sifraProjekta, sifraAktivnosti, nazivAktivnosti, opisAktivnosti";
    }

    @Override
    public String getValuesForInsert() {
        StringBuilder sb = new StringBuilder();
        sb.append("'").append(getProjekat().getSifraProjekta()).append("'").append(",")
                .append("'").append(getSifraAktivnosti()).append("'").append(",")
                .append("'").append(getNazivAktivnosti()).append("'").append(",")
                .append("'").append(getOpisAktivnosti()).append("'");
        String values = sb.toString();
        return values;
    }

    @Override
    public void setId(int id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getColumnNameAndValuesForUpdate() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<IDomainObject> makeList(ResultSet rs, Object o) throws Exception {
        Projekat p = (Projekat) o;
        List<IDomainObject> aktivnosti = new ArrayList<>();
        while (rs.next()) {
        
            String sifraA = rs.getString("sifraAktivnosti");
            String nazivA = rs.getString("nazivAktivnosti");
            String opisA = rs.getString("opisAktivnosti");

            Aktivnost akt = new Aktivnost();
            akt.setSifraAktivnosti(sifraA);
            akt.setProjekat(p);
            akt.setNazivAktivnosti(nazivA);
            akt.setOpisAktivnosti(opisA);

            aktivnosti.add(akt);
        }
        return aktivnosti;
    }

    @Override
    public String getKeyValue2(Object o) {
        Projekat p = (Projekat) o;
        return "'"+p.getSifraProjekta()+"'";
    }

    @Override
    public String getKeyValue(Object o) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int getNumberOfKeys() {
        return 2;
    }

    @Override
    public boolean autoincrement() {
        return false;
    }
    
    
}
