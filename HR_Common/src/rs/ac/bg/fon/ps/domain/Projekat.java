/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs.ac.bg.fon.ps.domain;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

/**
 *
 * @author Anja
 */
public class Projekat implements IDomainObject {

    private String sifraProjekta;
    private String nazivProjekta;
    private Date datumOdrzavanja;
    private ClanOrganizacije koordinatorProjekta;
    private List<Aktivnost> aktivnosti;

    public Projekat() {
        this.aktivnosti = new ArrayList<>();
    }

    public Projekat(String sifraProjekta, String nazivProjekta, Date datumOdrzavanja, ClanOrganizacije koordinatorProjekta) {
        this.sifraProjekta = sifraProjekta;
        this.nazivProjekta = nazivProjekta;
        this.datumOdrzavanja = datumOdrzavanja;
        this.koordinatorProjekta = koordinatorProjekta;
        this.aktivnosti = new ArrayList<>();
    }

    public String getSifraProjekta() {
        return sifraProjekta;
    }

    public void setSifraProjekta(String sifraProjekta) {
        this.sifraProjekta = sifraProjekta;
    }

    public String getNazivProjekta() {
        return nazivProjekta;
    }

    public void setNazivProjekta(String nazivProjekta) {
        this.nazivProjekta = nazivProjekta;
    }

    public Date getDatumOdrzavanja() {
        return datumOdrzavanja;
    }

    public void setDatumOdrzavanja(Date datumOdrzavanja) {
        this.datumOdrzavanja = datumOdrzavanja;
    }

    public ClanOrganizacije getKoordinatorProjekta() {
        return koordinatorProjekta;
    }

    public void setKoordinatorProjekta(ClanOrganizacije koordinatorProjekta) {
        this.koordinatorProjekta = koordinatorProjekta;
    }

    public List<Aktivnost> getAktivnosti() {
        return aktivnosti;
    }

    public void setAktivnosti(List<Aktivnost> aktivnosti) {
        this.aktivnosti = aktivnosti;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Projekat other = (Projekat) obj;
        if (!Objects.equals(this.sifraProjekta, other.sifraProjekta)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return nazivProjekta;
    }

    @Override
    public String getTableName() {
        return "projekat";
    }

    @Override
    public int getNumberConnectedObject() {
        return 2;
    }

    @Override
    public String getJoinCondition() {
        StringBuilder sb = new StringBuilder();
        sb.append(" p JOIN clan_organizacije c ON (p.koordinatorProjektaID = c.clanID)");

        return sb.toString();
    }

    @Override
    public String getWhereCondition() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getKeyValue() {
        return "'" + getSifraProjekta() + "'";
    }

    @Override
    public String getKeyName() {
        return "sifraProjekta";
    }

    @Override
    public IDomainObject makeForOne(ResultSet rs) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<IDomainObject> makeList(ResultSet rs) throws SQLException {
        List<IDomainObject> projekti = new ArrayList<>();
        while (rs.next()) {
            String sifraP = rs.getString("sifraProjekta");
            String nazivP = rs.getString("nazivProjekta");
            Date datum = rs.getDate("datumOdrzavanja");
            int koordinatorID = rs.getInt("koordinatorProjektaID");

            ClanOrganizacije cl = new ClanOrganizacije();
            cl.setClanID(koordinatorID);
            cl.setImePrezime(rs.getString("imePrezime"));
            cl.setKontakt(rs.getString("kontakt"));
            cl.setDatumUclanjenja(rs.getDate("datumUclanjenja"));
            cl.setDatumIsclanjenja(rs.getDate("datumIsclanjenja"));

            Projekat p = new Projekat();
            p.setSifraProjekta(sifraP);
            p.setNazivProjekta(nazivP);
            p.setDatumOdrzavanja(datum);
            p.setKoordinatorProjekta(cl);

            projekti.add(p);
        }
        return projekti;
    }

    @Override
    public String getKeyName2() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getKeyValue2() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getKeyName3() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getKeyValue3() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getAllColumnNames() {
        return "sifraProjekta, nazivProjekta, datumOdrzavanja, koordinatorProjektaID";
    }

    @Override
    public String getValuesForInsert() {
        StringBuilder sb = new StringBuilder();
        sb.append("'").append(getSifraProjekta()).append("'").append(",")
                .append("'").append(getNazivProjekta()).append("'").append(",")
                .append("'").append(new java.sql.Date(getDatumOdrzavanja().getTime())).append("'").append(",")
                .append("'").append(getKoordinatorProjekta().getClanID()).append("'");
        String values = sb.toString();
        return values;
    }

    @Override
    public void setId(int id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getColumnNameAndValuesForUpdate() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<IDomainObject> makeList(ResultSet rs, Object o) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getKeyValue2(Object o) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getKeyValue(Object o) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int getNumberOfKeys() {
        return 1;
    }

    @Override
    public boolean autoincrement() {
        return false;
    }

}
