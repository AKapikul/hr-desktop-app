/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs.ac.bg.fon.ps.domain.enumeration;

import java.io.Serializable;

/**
 *
 * @author Anja
 */
public enum FrmMode implements Serializable{
    FORM_ADD, FORM_UPDATE, FORM_VIEW
}
