/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs.ac.bg.fon.ps.domain;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 *
 * @author Anja
 */
public class Zadatak implements IDomainObject {

    private Projekat projekat;
    private Aktivnost aktivnost;
    private ClanOrganizacije izvrsitelj;
    private String opisZadatka;
    private int brojBodova;

    public Zadatak() {
    }

    public Zadatak(Projekat projekat, Aktivnost aktivnost, ClanOrganizacije izvrsitelj, String opisZadatka, int brojBodova) {
        this.projekat = projekat;
        this.aktivnost = aktivnost;
        this.izvrsitelj = izvrsitelj;
        this.opisZadatka = opisZadatka;
        this.brojBodova = brojBodova;
    }

    public Projekat getProjekat() {
        return projekat;
    }

    public void setProjekat(Projekat projekat) {
        this.projekat = projekat;
    }

    public Aktivnost getAktivnost() {
        return aktivnost;
    }

    public void setAktivnost(Aktivnost aktivnost) {
        this.aktivnost = aktivnost;
    }

    public ClanOrganizacije getIzvrsitelj() {
        return izvrsitelj;
    }

    public void setIzvrsitelj(ClanOrganizacije izvrsitelj) {
        this.izvrsitelj = izvrsitelj;
    }

    public String getOpisZadatka() {
        return opisZadatka;
    }

    public void setOpisZadatka(String opisZadatka) {
        this.opisZadatka = opisZadatka;
    }

    public int getBrojBodova() {
        return brojBodova;
    }

    public void setBrojBodova(int brojBodova) {
        this.brojBodova = brojBodova;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Zadatak other = (Zadatak) obj;
        if (!Objects.equals(this.projekat, other.projekat)) {
            return false;
        }
        if (!Objects.equals(this.aktivnost, other.aktivnost)) {
            return false;
        }
        if (!Objects.equals(this.izvrsitelj, other.izvrsitelj)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return opisZadatka;
    }

    @Override
    public String getTableName() {
        return "zadatak";
    }

    @Override
    public int getNumberConnectedObject() {
        return 3;
    }

    @Override
    public String getJoinCondition() {
        return " z JOIN clan_organizacije c ON (z.izvrsiteljID = c.clanID) JOIN sektor s ON (c.sifraSektora = s.sifraSektora)";
    }

    @Override
    public String getWhereCondition() {
        return "";
    }

    @Override
    public String getKeyValue() {
        return "'" + getAktivnost().getSifraAktivnosti() + "'";
    }

    @Override
    public String getKeyName() {
        return "sifraAktivnosti";
    }

    @Override
    public IDomainObject makeForOne(ResultSet rs) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<IDomainObject> makeList(ResultSet rs) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getKeyName2() {
        return "sifraProjekta";
    }

    @Override
    public String getKeyValue2() {
        return "'" + getProjekat().getSifraProjekta() + "'";
    }

    @Override
    public String getKeyName3() {
        return "izvrsiteljID";
    }

    @Override
    public String getKeyValue3() {
        return getIzvrsitelj().getClanID() + "";
    }

    @Override
    public String getAllColumnNames() {
        return "sifraProjekta, sifraAktivnosti, izvrsiteljID, opisZadatka, brojBodova";
    }

    @Override
    public String getValuesForInsert() {
        StringBuilder sb = new StringBuilder();
        sb.append("'").append(getProjekat().getSifraProjekta()).append("'").append(",")
                .append("'").append(getAktivnost().getSifraAktivnosti()).append("'").append(",")
                .append(getIzvrsitelj().getClanID()).append(",")
                .append("'").append(getOpisZadatka()).append("'").append(",")
                .append(getBrojBodova());
        String values = sb.toString();
        return values;
    }

    @Override
    public void setId(int id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getColumnNameAndValuesForUpdate() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<IDomainObject> makeList(ResultSet rs, Object o) throws Exception {
        Aktivnost a = (Aktivnost) o;
        List<IDomainObject> zadaci = new ArrayList<>();
        while (rs.next()) {

            String opis = rs.getString("opisZadatka");
            int brb = rs.getInt("brojBodova");

            ClanOrganizacije i = new ClanOrganizacije();
            i.setClanID(rs.getInt("izvrsiteljID"));
            i.setImePrezime(rs.getString("imePrezime"));
            i.setKontakt(rs.getString("kontakt"));
            i.setDatumUclanjenja(rs.getDate("datumUclanjenja"));
            i.setDatumIsclanjenja(rs.getDate("datumIsclanjenja"));

            Sektor s = new Sektor();
            s.setSifraSektora(rs.getString("sifraSektora"));
            s.setPunNaziv(rs.getString("punNaziv"));
            s.setKoordinatorSektoraID(rs.getInt("clanID"));

            i.setSektor(s);

            Zadatak z = new Zadatak();
            z.setAktivnost(a);
            z.setProjekat(a.getProjekat());
            z.setIzvrsitelj(i);
            z.setOpisZadatka(opis);
            z.setBrojBodova(brb);

            zadaci.add(z);
        }
        return zadaci;
    }

    @Override
    public String getKeyValue2(Object o) {
        Aktivnost a = (Aktivnost) o;
        return "'" + a.getProjekat().getSifraProjekta() + "'";
    }

    @Override
    public String getKeyValue(Object o) {
        Aktivnost a = (Aktivnost) o;
        return "'" + a.getSifraAktivnosti() + "'";
    }

    @Override
    public int getNumberOfKeys() {
        return 3;
    }

    @Override
    public boolean autoincrement() {
        return false;
    }

}
