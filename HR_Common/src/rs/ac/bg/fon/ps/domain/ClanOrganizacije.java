/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs.ac.bg.fon.ps.domain;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Anja
 */
public class ClanOrganizacije implements IDomainObject {

    private int clanID;
    private String imePrezime;
    private Date datumUclanjenja;
    private Date datumIsclanjenja;
    private Sektor sektor;
    private String kontakt;
    private int brojBodova;

    public ClanOrganizacije() {
    }

    public ClanOrganizacije(int clanID, String imePrezime, Date datumUclanjenja, Date datumIsclanjenja, Sektor sektor, String kontakt, int brojBodova) {
        this.clanID = clanID;
        this.imePrezime = imePrezime;
        this.datumUclanjenja = datumUclanjenja;
        this.datumIsclanjenja = datumIsclanjenja;
        this.sektor = sektor;
        this.kontakt = kontakt;
        this.brojBodova = brojBodova;
    }

    public int getBrojBodova() {
        return brojBodova;
    }

    public void setBrojBodova(int brojBodova) {
        this.brojBodova = brojBodova;
    }

    public int getClanID() {
        return clanID;
    }

    public void setClanID(int clanID) {
        this.clanID = clanID;
    }

    public String getImePrezime() {
        return imePrezime;
    }

    public void setImePrezime(String imePrezime) {
        this.imePrezime = imePrezime;
    }

    public Date getDatumUclanjenja() {
        return datumUclanjenja;
    }

    public void setDatumUclanjenja(Date datumUclanjenja) {
        this.datumUclanjenja = datumUclanjenja;
    }

    public Sektor getSektor() {
        return sektor;
    }

    public void setSektor(Sektor sektor) {
        this.sektor = sektor;
    }

    public Date getDatumIsclanjenja() {
        return datumIsclanjenja;
    }

    public void setDatumIsclanjenja(Date datumIsclanjenja) {
        this.datumIsclanjenja = datumIsclanjenja;
    }

    public String getKontakt() {
        return kontakt;
    }

    public void setKontakt(String kontakt) {
        this.kontakt = kontakt;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ClanOrganizacije other = (ClanOrganizacije) obj;
        if (this.clanID != other.clanID) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return imePrezime;
    }

    @Override
    public String getTableName() {
        return "clan_organizacije";
    }

    @Override
    public int getNumberConnectedObject() {
        return 2;
    }

    @Override
    public String getJoinCondition() {
        StringBuilder sb = new StringBuilder();
        sb.append(" c JOIN sektor s ON (c.sifraSektora = s.sifraSektora) ORDER BY brojBodova DESC");

        return sb.toString();
    }

    @Override
    public String getWhereCondition() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getKeyValue() {
        return getClanID() + "";
    }

    @Override
    public String getKeyName() {
        return "clanID";
    }

    @Override
    public IDomainObject makeForOne(ResultSet rs) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<IDomainObject> makeList(ResultSet rs) throws Exception {
        List<IDomainObject> clanovi = new ArrayList<>();
        while (rs.next()) {
            int clanID = rs.getInt("clanID");
            String ime = rs.getString("imePrezime");
            Date datumUcl = rs.getDate("datumUclanjenja");
            Date datumIscl = rs.getDate("datumIsclanjenja");
            String sifraSektor = rs.getString("sifraSektora");
            String kont = rs.getString("kontakt");
            int brB = rs.getInt("brojBodova");

            Sektor s = new Sektor();
            s.setSifraSektora(sifraSektor);
            s.setPunNaziv(rs.getString("punNaziv"));
            s.setKoordinatorSektoraID(rs.getInt("koordinatorSektoraID"));

            ClanOrganizacije clan = new ClanOrganizacije();
            clan.setClanID(clanID);
            clan.setImePrezime(ime);
            clan.setDatumUclanjenja(datumUcl);
            clan.setDatumIsclanjenja(datumIscl);
            clan.setSektor(s);
            clan.setKontakt(kont);
            clan.setBrojBodova(brB);

            clanovi.add(clan);
        }
        return clanovi;
    }

    @Override
    public String getKeyName2() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getKeyValue2() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getKeyName3() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getKeyValue3() {
        return "brojBodova = " + getBrojBodova() + "";
    }

    @Override
    public String getAllColumnNames() {
        if (getDatumIsclanjenja() == null) {
            return "imePrezime, datumUclanjenja, sifraSektora, kontakt";
        }
        return "imePrezime, datumUclanjenja, datumIsclanjenja, sifraSektora, kontakt";
    }

    @Override
    public String getValuesForInsert() {
        StringBuilder sb = new StringBuilder();

        sb.append("'").append(getImePrezime()).append("'").append(",")
                .append("'").append(new java.sql.Date(getDatumUclanjenja().getTime())).append("'");
        if (getDatumIsclanjenja() == null) {
            sb.append(",")
                    .append("'").append(getSektor().getSifraSektora()).append("'").append(",")
                    .append("'").append(getKontakt()).append("'");
        } else {
            sb.append(",")
                    .append("'").append(new java.sql.Date(getDatumIsclanjenja().getTime())).append("'").append(",")
                    .append("'").append(getSektor().getSifraSektora()).append("'").append(",")
                    .append("'").append(getKontakt()).append("'");
        }

        String values = sb.toString();
        return values;
    }

    @Override
    public void setId(int id) {
        this.clanID = id;
    }

    @Override
    public String getColumnNameAndValuesForUpdate() {
        if (getDatumIsclanjenja() == null) {
            return "imePrezime = '" + getImePrezime() + "', datumUclanjenja = '" + new java.sql.Date(getDatumUclanjenja().getTime())
                    + "', datumIsclanjenja = " + null + ", sifraSektora = '" + getSektor().getSifraSektora()
                    + "', kontakt = '" + getKontakt() + "'";
        } else {
            return "imePrezime = '" + getImePrezime() + "', datumUclanjenja = '" + new java.sql.Date(getDatumUclanjenja().getTime())
                    + "', datumIsclanjenja = '" + new java.sql.Date(getDatumIsclanjenja().getTime()) + "', sifraSektora = '" + getSektor().getSifraSektora()
                    + "', kontakt = '" + getKontakt() + "'";
        }
    }

    @Override
    public List<IDomainObject> makeList(ResultSet rs, Object o) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getKeyValue2(Object o) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getKeyValue(Object o) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int getNumberOfKeys() {
        return 1;
    }

    @Override
    public boolean autoincrement() {
        return true;
    }

}
