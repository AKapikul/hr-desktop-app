/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs.ac.bg.fon.ps.domain;

import java.io.Serializable;
import java.sql.ResultSet;
import java.util.List;

/**
 *
 * @author Anja
 */
public interface IDomainObject extends Serializable {

    public String getTableName();

    public int getNumberConnectedObject();

    public String getJoinCondition();

    public String getWhereCondition();

    public String getKeyValue();

    public String getKeyName();

    public IDomainObject makeForOne(ResultSet rs) throws Exception;

    public List<IDomainObject> makeList(ResultSet rs)  throws Exception;

    public String getKeyName2();

    public String getKeyValue2();

    public String getKeyName3();

    public String getKeyValue3();

    public String getAllColumnNames();

    public String getValuesForInsert();

    public void setId(int id);

    public String getColumnNameAndValuesForUpdate();

    public List<IDomainObject> makeList(ResultSet rs, Object o) throws Exception;

    public String getKeyValue2(Object o);

    public String getKeyValue(Object o);

    public int getNumberOfKeys();

    public boolean autoincrement();


    

    
}
