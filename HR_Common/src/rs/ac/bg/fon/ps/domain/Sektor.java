/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs.ac.bg.fon.ps.domain;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 *
 * @author Anja
 */
public class Sektor implements IDomainObject {

    private String sifraSektora;
    private String punNaziv;
    // private ClanOrganizacije koordinatorSektora;
    private int koordinatorSektoraID;

    public Sektor() {
    }

    public Sektor(String sifraSektora, String punNaziv, int koordinatorSektoraID) {
        this.sifraSektora = sifraSektora;
        this.punNaziv = punNaziv;
        this.koordinatorSektoraID = koordinatorSektoraID;
    }

    public String getSifraSektora() {
        return sifraSektora;
    }

    public void setSifraSektora(String sifraSektora) {
        this.sifraSektora = sifraSektora;
    }

    public String getPunNaziv() {
        return punNaziv;
    }

    public void setPunNaziv(String punNaziv) {
        this.punNaziv = punNaziv;
    }

    public int getKoordinatorSektoraID() {
        return koordinatorSektoraID;
    }

    public void setKoordinatorSektoraID(int koordinatorSektoraID) {
        this.koordinatorSektoraID = koordinatorSektoraID;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Sektor other = (Sektor) obj;
        if (!Objects.equals(this.sifraSektora, other.sifraSektora)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return sifraSektora;
    }

    @Override
    public String getTableName() {
        return "sektor";
    }

    @Override
    public int getNumberConnectedObject() {
        return 0;
    }

    @Override
    public String getJoinCondition() {
//        StringBuilder sb = new StringBuilder();
//        sb.append(" s JOIN clan_organizacije c ON (s.koordinatorSektoraID = c.clanID)");
//        return sb.toString();
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public String getWhereCondition() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getKeyValue() {
        return "'"+getSifraSektora()+"'";
    }

    @Override
    public String getKeyName() {
        return "sifraSektora";
    }

    @Override
    public IDomainObject makeForOne(ResultSet rs) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<IDomainObject> makeList(ResultSet rs) throws SQLException {
        List<IDomainObject> sektori = new ArrayList<>();
        while (rs.next()) {
            String sifra = rs.getString("sifraSektora");
            String punNaz = rs.getString("punNaziv");
            int koor = rs.getInt("koordinatorSektoraID");

//            ClanOrganizacije c = new ClanOrganizacije();
//            c.setClanID(koor);
//            c.setDatumUclanjenja(rs.getDate("datumUclanjenja"));
//            c.setDatumIsclanjenja(rs.getDate("datumIsclanjenja"));
//            c.setImePrezime(rs.getString("imePrezime"));
//            c.setKontakt(rs.getString("kontakt"));
            Sektor sek = new Sektor();
            sek.setSifraSektora(sifra);
            sek.setPunNaziv(punNaz);
            sek.setKoordinatorSektoraID(koor);

            sektori.add(sek);
        }
        return sektori;
    }

    @Override
    public String getKeyName2() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getKeyValue2() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getKeyName3() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getKeyValue3() {
        return "koordinatorSektoraID";
    }

    @Override
    public String getAllColumnNames() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getValuesForInsert() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void setId(int id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getColumnNameAndValuesForUpdate() {
        return "koordinatorSektoraID = " + getKoordinatorSektoraID();
                    
    }

    

    @Override
    public List<IDomainObject> makeList(ResultSet rs, Object o) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getKeyValue2(Object o) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getKeyValue(Object o) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int getNumberOfKeys() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean autoincrement() {
        return false;
    }

}
